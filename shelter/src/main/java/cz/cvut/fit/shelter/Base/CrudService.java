package cz.cvut.fit.shelter.Base;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.Optional;


public abstract class CrudService <T extends DataID<ID>, ID, R extends CrudRepository<T, ID>>{
    protected final R repository;

    public CrudService(R repository) {
        this.repository = repository;
    }

    public T create(T data) {
        if (data.getId() != null && repository.existsById(data.getId()))
            throw new EntityExistsException("Entity with id: " + data.getId() + " already exists");
        data.setTimeOfUpdated(LocalDateTime.now());
        return repository.save(data);
    }

    public Iterable<T> readAll() {
        return repository.findAll();
    }

    public Optional<T> readById(ID id) {
        return repository.findById(id);
    }

    public void update(ID id, T data) {
        if (!repository.existsById(id))
            throw new EntityNotFoundException("Entity with id: " + id + " not found");
        data.setId(id);
        data.setTimeOfUpdated(LocalDateTime.now());
        repository.save(data);
    }

    public void deleteById(ID id) {
        if (!repository.existsById(id))
            throw new EntityNotFoundException("Entity with id: " + id + " not found");
        repository.deleteById(id);
    }


}
