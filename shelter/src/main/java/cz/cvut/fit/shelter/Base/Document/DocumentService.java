package cz.cvut.fit.shelter.Base.Document;

import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.Animal.AnimalService;
import cz.cvut.fit.shelter.Base.User.User;
import cz.cvut.fit.shelter.Base.User.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DocumentService {
    private final DocumentRepository documentRepository;
    private final UserService userService;
    private final AnimalService animalService;

    public void upload(MultipartFile file, Long idStaff, Long idAnimal) throws IOException {
        Document document = multipartFileToDocument(file);
        Optional<User> staff = userService.readById(idStaff);
        document.setCreator(staff.orElseThrow());
        Optional<Animal> animal = animalService.readById(idAnimal);
        document.setAnimal(animal.orElseThrow());
        documentRepository.save(document);
    }

    public Document findById(Long id) {
        return documentRepository.findById(id).orElseThrow();
    }

    public void deleteById(Long id) {
        documentRepository.deleteById(id);
    }

    private Document multipartFileToDocument(MultipartFile file) throws IOException {
        Document document = new Document();
        document.setName(file.getOriginalFilename());
        document.setContentType(file.getContentType());
        document.setSize(file.getSize());
        document.setFile(file.getBytes());
        return document;
    }
}
