package cz.cvut.fit.shelter.configuration;

import cz.cvut.fit.shelter.Base.User.Role;
import cz.cvut.fit.shelter.Base.User.Security.RegisterRequest;
import cz.cvut.fit.shelter.Base.User.UserRepository;
import cz.cvut.fit.shelter.Base.User.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.EnumSet;

@Component
@RequiredArgsConstructor
public class CLR implements CommandLineRunner {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepo;
    @Override
    public void run(String... args) throws Exception {
        if(userRepo.noUserWithRoleExists(Role.ADMIN)){
            var user = RegisterRequest.builder()
                            .name(Constants.SUPER_USER_NAME)
                            .surname(Constants.SUPER_USER_SURNAME)
                            .email(Constants.SUPER_USER_EMAIL)
                            .password(Constants.SUPER_USER_PASSWORD)
                            .roles(EnumSet.allOf(Role.class))
                            .build();
            userService.register(user);
        }
    }
}
