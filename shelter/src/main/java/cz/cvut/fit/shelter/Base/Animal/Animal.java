package cz.cvut.fit.shelter.Base.Animal;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Document.Document;
import cz.cvut.fit.shelter.Base.Room.Enclosure.Enclosure;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Animal implements DataID<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAnimal;

    @ManyToOne
    @NotNull
    private Enclosure enclosure;
    private String name;
    private String species;
    private String breed;
    private LocalDateTime birthDate;
    private LocalDateTime arrivalDate;
    private Sex sex;
    private String description;
    private Float weight;
    private Status status;
    @OneToMany(mappedBy = "animal", fetch = FetchType.LAZY)
    private List<Document> documents;

    private LocalDateTime timeOfUpdated;

    @PrePersist
    void init() {
        timeOfUpdated = LocalDateTime.now();
    }
    @Override
    public Long getId() {
        return idAnimal;
    }
    @Override
    public void setId(Long id) {
        this.idAnimal = id;
    }
}
