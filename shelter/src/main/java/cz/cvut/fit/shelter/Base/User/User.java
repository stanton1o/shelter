package cz.cvut.fit.shelter.Base.User;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Document.Document;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "staff")
public class User implements DataID<Long>, UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;
    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    @Column(unique = true)
    private String email;
    @NotBlank
    private String password;
    private String phone;
    private Float salary;
    @OneToMany(mappedBy = "creator", fetch = FetchType.LAZY)
    private List<Document> documents;
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;
    private LocalDateTime timeOfUpdated;
    private boolean active;

    @Override
    public Long getId() {
        return idUser;
    }

    @Override
    public void setId(Long id) {
        this.idUser = id;
    }

    @PrePersist
    void init() {
        timeOfUpdated = LocalDateTime.now();
        if(roles == null)
            roles = EnumSet.of(Role.USER);
        else
            roles.add(Role.USER);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
