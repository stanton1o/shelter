package cz.cvut.fit.shelter.Base.Item.DTOs;

import cz.cvut.fit.shelter.Base.DataID;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@Schema(name = "ItemDTO", description = "Data transfer object for item")
public class ItemDTO implements DataID<Long> {
    @Schema(description = "Unique identifier of the item", example = "1", required = true)
    private Long idItem;
    @Schema(description = "Name of the item", example = "Table", required = true)
    private String name;
    @Schema(description = "Description of the item", example = "Table for living room")
    private String description;
    @Schema(description = "Room id where item is", example = "32", required = true)
    private Long idRoom;

    @Schema(description = "Time of item update", example = "2021-12-31T23:59:59.999999999")
    private LocalDateTime timeOfUpdated;
    @Override
    @Schema(hidden = true)
    public Long getId() {
        return idItem;
    }

    @Override
    @Schema(hidden = true)
    public void setId(Long aLong) {
        this.idItem = aLong;
    }
}
