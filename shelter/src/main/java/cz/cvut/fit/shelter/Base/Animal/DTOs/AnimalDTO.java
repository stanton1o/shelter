package cz.cvut.fit.shelter.Base.Animal.DTOs;


import cz.cvut.fit.shelter.Base.Animal.Sex;
import cz.cvut.fit.shelter.Base.Animal.Status;
import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Document.DTOs.DocumentDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@Schema(name = "AnimalDTO", description = "Data transfer object for animal")
public class AnimalDTO implements DataID<Long> {

    @Schema(description = "Unique identifier of the animal", example = "1", required = true)
    private Long idAnimal;
    @Schema(description = "Room id where animal is", example = "32")
    private Long idRoom;
    @Schema(description = "Name of the animal", example = "Rex", required = true)
    private String name;
    @Schema(description = "Species of the animal", example = "Dog", required = true)
    private String species;
    @Schema(description = "Breed of the animal", example = "Labrador")
    private String breed;
    @Schema(description = "Birth date of the animal", example = "2020-01-01", required = true)
    private LocalDateTime birthDate;
    @Schema(description = "Arrival date of the animal", example = "2020-01-01", required = true)
    private LocalDateTime arrivalDate;
    @Schema(description = "Animal sex", example = "Male")
    private Sex sex;
    @Schema(description = "Description of the animal", example = "Dog")
    private String description;
    @Schema(description = "Weight of the animal", example = "20.0")
    private Float weight;
    @Schema(description = "Animal status", example = "AVAILABLE", required = true)
    private Status status;
    @Schema(description = "Documents which are attached to the animal", example = "[{\"idDocument\":1,\"name\":\"Document\",\"contentType\":\"Document for animal\",\"path\":\"C:/Users/Document.pdf\",\"size\":1000,\"animal\":1,\"creator\":1}]")
    private List<DocumentDTO> documents;
    @Schema(description = "Time of animal update", example = "2021-12-31T23:59:59.999999999")
    private LocalDateTime timeOfUpdated;

    @Override
    @Schema(hidden = true)
    public Long getId() {
        return idAnimal;
    }
    @Override
    @Schema(hidden = true)
    public void setId(Long id) {
        this.idAnimal = id;
    }
}
