package cz.cvut.fit.shelter.Base.Document.DTOs;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalMapper;
import cz.cvut.fit.shelter.Base.Document.Document;
import cz.cvut.fit.shelter.Base.User.DTOs.UserMapper;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
@Mapper(componentModel = "spring", uses = {AnimalMapper.class, UserMapper.class})
public interface DocumentMapper extends SuperMapper<Document, DocumentDTO> {

    @Override
    @Mapping(source = "document.idDocument", target = "idDocument")
    @Mapping(source = "creator.idUser", target = "creator")
    @Mapping(source = "animal.idAnimal", target = "animal")
    DocumentDTO toDTO(Document document);

    @Override
    @Mapping(source = "creator", target = "creator.idUser")
    @Mapping(source = "animal", target = "animal.idAnimal")
    @Mapping(target = "file", ignore = true)
    Document toEntity(DocumentDTO documentDTO);

    @Override
    Iterable<DocumentDTO> toDTO(Iterable<Document> documents);

    @Override
    Iterable<Document> toEntity(Iterable<DocumentDTO> documentDTOs);
}
