package cz.cvut.fit.shelter.Base;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalDTO;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemDTO;
import cz.cvut.fit.shelter.Base.Room.DTOs.EnclosureDTO;
import cz.cvut.fit.shelter.Base.Room.DTOs.RoomDTO;
import cz.cvut.fit.shelter.Base.User.DTOs.UserDTO;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(description = "Data transfer object for all subclasses", subTypes = {UserDTO.class, RoomDTO.class, EnclosureDTO.class, ItemDTO.class, AnimalDTO.class})
public interface DataID <ID>{
    ID getId();

    void setId(ID id);

    void setTimeOfUpdated(LocalDateTime timeOfUpdated);
}
