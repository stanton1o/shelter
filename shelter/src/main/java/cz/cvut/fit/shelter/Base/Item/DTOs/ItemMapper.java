package cz.cvut.fit.shelter.Base.Item.DTOs;

import cz.cvut.fit.shelter.Base.Item.Item;
import cz.cvut.fit.shelter.Base.Room.Room;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ItemMapper extends SuperMapper<Item, ItemDTO> {
    @Override
    @Mapping(source = "room.idRoom", target = "idRoom")
    ItemDTO toDTO(Item item);
    @Override
    @Mapping(source = "idRoom", target = "room")
    Item toEntity(ItemDTO itemDTO);

    @Override
    Iterable<ItemDTO> toDTO(Iterable<Item> items);

    @Override
    Iterable<Item> toEntity(Iterable<ItemDTO> itemDTOs);

    default Room toRoom(Long idRoom){
        Room room = new Room();
        room.setId(idRoom);
        return room;
    }

    @Mapping(source = "room.idRoom", target = "idRoom")
    ReadAllItemsResponse toReadAllItemsResponse(Item item);

    Iterable<ReadAllItemsResponse> toReadAllItemsResponse(Iterable<Item> items);
}
