package cz.cvut.fit.shelter.Base.Item;

import cz.cvut.fit.shelter.Base.CrudController;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemDTO;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemMapper;
import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.CURRENT_API_VERSION +"/items")
@Tag(name = "ItemController", description = "API which provides operations for item")
public class ItemController extends CrudController<Item, Long,ItemService, ItemRepository, ItemMapper, ItemDTO> {
    public ItemController(ItemService service, ItemMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/show")
    @Operation(summary = "Show items users with the main information")
    @ApiResponse(responseCode = "200", description = "Items were found successfully", content = @Content(schema = @Schema(implementation = ItemDTO.class)))
    public ResponseEntity<?> showAll() {
        return ResponseEntity.ok().body(mapper.toReadAllItemsResponse(service.readAll()));
    }
}
