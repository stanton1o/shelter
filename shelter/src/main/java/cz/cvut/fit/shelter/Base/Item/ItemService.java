package cz.cvut.fit.shelter.Base.Item;

import cz.cvut.fit.shelter.Base.CrudService;
import org.springframework.stereotype.Service;

@Service
public class ItemService extends CrudService<Item, Long, ItemRepository> {
    public ItemService(ItemRepository repository) {
        super(repository);
    }
}
