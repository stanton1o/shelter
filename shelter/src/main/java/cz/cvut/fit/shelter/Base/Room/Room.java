package cz.cvut.fit.shelter.Base.Room;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Item.Item;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public class Room implements DataID<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long idRoom;

    protected String name;

    protected String description;

    @NotNull
    protected Float area;

    @NotNull
    protected Float high;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    protected List<Item> items = new ArrayList<>();

    protected LocalDateTime timeOfUpdated;
    @Override
    public Long getId() {
        return idRoom;
    }
    @Override
    public void setId(Long id) {
        this.idRoom = id;
    }
}
