package cz.cvut.fit.shelter.Base.Animal;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalDTO;
import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalMapper;
import cz.cvut.fit.shelter.Base.CrudController;
import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;


@RestController
@RequestMapping(Constants.CURRENT_API_VERSION +"/animals")
@Tag(name = "AnimalController", description = "API which provides operations for animal")
public class AnimalController extends
        CrudController<Animal, Long, AnimalService, AnimalRepository, AnimalMapper, AnimalDTO> {

        public AnimalController(AnimalService service, AnimalMapper mapper) {
            super(service, mapper);
        }

        @GetMapping("/statuses")
        @Operation(summary = "Return all statuses of animals")
        @ApiResponse(responseCode = "200", description = "All statuses were returned successfully", content = @Content(schema = @Schema(implementation = Status.class)))
        public String[] getStatuses() {
            //get values as string of enum Status
            return Arrays.stream((Status.values())).map(Status::name).toArray(String[]::new);
        }

    @GetMapping("/show")
    @Operation(summary = "Show all animals with the main information")
    @ApiResponse(responseCode = "200", description = "Animals were found successfully", content = @Content(schema = @Schema(implementation = AnimalDTO.class)))
    public ResponseEntity<?> showAll() {
        return ResponseEntity.ok().body(mapper.toReadAllAnimalsResponse(service.readAll()));
    }
}
