package cz.cvut.fit.shelter.Base.Room.DTOs;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalMapper;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemMapper;
import cz.cvut.fit.shelter.Base.Room.Room;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ItemMapper.class, AnimalMapper.class})
public interface RoomMapper extends SuperMapper<Room, RoomDTO> {
    @Override
    RoomDTO toDTO(Room room);
    @Override
    Room toEntity(RoomDTO roomDTO);

    @Override
    Iterable<RoomDTO> toDTO(Iterable<Room> rooms);

    @Override
    Iterable<Room> toEntity(Iterable<RoomDTO> roomDTOs);
    ReadAllRoomsResponse toReadAllRoomsResponse(Room room);

    Iterable<ReadAllRoomsResponse> toReadAllRoomsResponse(Iterable<Room> rooms);

}
