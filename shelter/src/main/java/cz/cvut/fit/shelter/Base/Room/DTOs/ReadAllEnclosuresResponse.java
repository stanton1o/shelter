package cz.cvut.fit.shelter.Base.Room.DTOs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "ReadAllEnclosuresResponse", description = "Response for showing all enclosures")
public class ReadAllEnclosuresResponse {

    @Schema(description = "Unique identifier of the enclosure", example = "1", required = true)
    private Long id;
    @Schema(description = "Name of the enclosure", example = "Lion enclosure", required = true)
    private String name;
    @Schema(description = "Description of the enclosure", example = "Enclosure for lions")
    private String description;
    @Schema(description = "Area of the enclosure", example = "200.0")
    private Float area;
    @Schema(description = "Height of the enclosure", example = "5.0")
    private Float high;

}
