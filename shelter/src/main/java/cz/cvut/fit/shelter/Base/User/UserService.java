package cz.cvut.fit.shelter.Base.User;

import cz.cvut.fit.shelter.Base.CrudService;
import cz.cvut.fit.shelter.Base.User.Security.AuthenticationRequest;
import cz.cvut.fit.shelter.Base.User.Security.AuthenticationResponse;
import cz.cvut.fit.shelter.Base.User.Security.RegisterRequest;
import cz.cvut.fit.shelter.configuration.JwtService;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService extends CrudService<User, Long, UserRepository> {

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authManager;
    public UserService(UserRepository repository, PasswordEncoder passwordEncoder, JwtService jwtService, AuthenticationManager authManager) {
        super(repository);
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.authManager = authManager;
    }

    public Optional<User> findByEmail(String email) {
        return repository.findByEmail(email);
    }

    public AuthenticationResponse register(RegisterRequest request) {
        if(findByEmail(request.getEmail()).isPresent())
            throw new EntityExistsException("User with email: " + request.getEmail() + " already exists");
        var user = User.builder()
                .name(request.getName())
                .surname(request.getSurname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .roles(request.getRoles().isEmpty() ? EnumSet.of(Role.USER) : request.getRoles())
                .active(true)
                .build();
        repository.save(user);
        var token = jwtService.genToken(user);
        return AuthenticationResponse.builder().token(token).build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        var obj = findByEmail(request.getEmail());
        if(obj.isEmpty())
            throw new EntityNotFoundException("User with email: " + request.getEmail() + " does not exist");
        var token = jwtService.genToken(obj.get());
        System.out.println(request.getEmail() + " was logged in successfully");
        return AuthenticationResponse.builder().token(token).build();
    }

    @Override
    public void update(Long id, User data) {
        if (!repository.existsById(id))
            throw new EntityNotFoundException("Entity with id: " + id + " not found");
        if(!Objects.equals(data.getPassword(), repository.findById(id).get().getPassword()))
            data.setPassword(passwordEncoder.encode(data.getPassword()));
        data.setId(id);
        data.setTimeOfUpdated(LocalDateTime.now());
        repository.save(data);
    }


}
