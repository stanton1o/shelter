package cz.cvut.fit.shelter.Base.Room.Enclosure;

import cz.cvut.fit.shelter.Base.CrudController;
import cz.cvut.fit.shelter.Base.Room.DTOs.EnclosureDTO;
import cz.cvut.fit.shelter.Base.Room.DTOs.EnclosureMapper;
import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.CURRENT_API_VERSION +"/enclosures")
@Tag(name = "EnclosureController", description = "API which provides operations for enclosure")
public class EnclosureController extends CrudController<Enclosure, Long, EnclosureService, EnclosureRepository, EnclosureMapper, EnclosureDTO> {
    public EnclosureController(EnclosureService service, EnclosureMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/show")
    @Operation(summary = "Show all enclosures with the main information")
    @ApiResponse(responseCode = "200", description = "Enclosures were found successfully", content = @Content(schema = @Schema(implementation = EnclosureDTO.class)))
    public ResponseEntity<?> showAll() {
        return ResponseEntity.ok().body(mapper.toReadAllEnclosuresResponse(service.readAll()));
    }
}
