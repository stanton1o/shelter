package cz.cvut.fit.shelter.Base.User.DTOs;

import cz.cvut.fit.shelter.Base.User.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
@Schema(name = "ReadAllUsersResponse", description = "Response for showing all users")
public class ReadAllUsersResponse {

    @Schema(description = "Unique identifier of the user", example = "1", required = true)
    private Long id;
    @Schema(description = "Name of the user", example = "John", required = true)
    private String name;
    @Schema(description = "Surname of the user", example = "Doe", required = true)
    private String surname;
    @Schema(description = "Email and login of the user", example = "example@gmail.com", required = true)
    private String email;
    @Schema(description = "Roles of the user", example = "[\"USER\"]", required = true)
    private List<Role> roles;

}
