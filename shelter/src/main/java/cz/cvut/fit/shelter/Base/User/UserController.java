package cz.cvut.fit.shelter.Base.User;

import cz.cvut.fit.shelter.Base.CrudController;
import cz.cvut.fit.shelter.Base.Document.DocumentService;
import cz.cvut.fit.shelter.Base.User.DTOs.UserDTO;
import cz.cvut.fit.shelter.Base.User.DTOs.UserMapper;
import cz.cvut.fit.shelter.Base.User.Security.AuthenticationRequest;
import cz.cvut.fit.shelter.Base.User.Security.RegisterRequest;
import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@Controller
@RequestMapping(Constants.CURRENT_API_VERSION + "/staffs")
@Tag(name = "UserController", description = "API which provides operations for user(staff)")
public class UserController extends CrudController<User, Long, UserService, UserRepository, UserMapper, UserDTO> {

    public UserController(UserService service, UserMapper mapper, DocumentService documentService) {
        super(service, mapper);
        this.documentService = documentService;
    }

    private final DocumentService documentService;

    @PostMapping(value = "/{idStaff}/documents/{idAnimal}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Staff uploads document for animal")
    @ApiResponse(responseCode = "201", description = "Document was uploaded successfully")
    @ApiResponse(responseCode = "409", description = "Document was not uploaded")
    void uploadFile(@RequestPart("file") MultipartFile file,
                    @PathVariable Long idStaff,
                    @PathVariable Long idAnimal) {
        try {

            documentService.upload(file, idStaff, idAnimal);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @Override
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "Admin update user by id")
    @ApiResponse(responseCode = "204", description = "User was updated successfully")
    @ApiResponse(responseCode = "404", description = "User not found")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody UserDTO userDTO) {
        try {
            service.update(id, mapper.toEntity(userDTO));
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    @PostMapping
    @Operation(hidden = true)
    public ResponseEntity<?> create(@RequestBody UserDTO data) {
        return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY)
                .body("Use " + Constants.CURRENT_API_VERSION + "/staffs/register endpoint to create new user");

    }

    @PostMapping("/register")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "Admin register new user")
    @ApiResponse(responseCode = "200", description = "User was registered successfully",
            content = @Content(schema = @Schema(type = "string"), examples = @ExampleObject(value = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20ifQ.1")))
    @ApiResponse(responseCode = "403", description = "User with email is reserved")
    @ApiResponse(responseCode = "409", description = "User with email already exists")
    @ApiResponse(responseCode = "500", description = "Server error")
    public ResponseEntity<?> registerUser(@RequestBody RegisterRequest request) {
        if (request.getEmail().equals(Constants.SUPER_USER_EMAIL))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Email " + Constants.SUPER_USER_EMAIL + " is reserved: ");
        try {
            return ResponseEntity.ok(service.register(request));
        } catch (EntityExistsException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("User with email: " + request.getEmail() + " already exists");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
        }
}


    @PostMapping("/login")
    @Operation(summary = "User login by email and password")
    @ApiResponse(responseCode = "200",description = "User was logged in successfully",
            content = @Content(schema = @Schema(type = "string"), examples = @ExampleObject(value = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbkBnbWFpbC5jb20ifQ.1")))
    @ApiResponse(responseCode = "403",description = "User is blocked")
    @ApiResponse(responseCode = "409",description = "User does not exist")
    public ResponseEntity<?> loginUser( @RequestBody AuthenticationRequest request){
        var user = service.findByEmail(request.getEmail());
        if (user.isPresent() && !user.get().isActive())
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User with email: " + request.getEmail() + " is blocked");
        try {
            return ResponseEntity.ok(service.authenticate(request));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Your email or password is incorrect");
        }
    }


    @Override
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    @Operation(summary = "Admin delete user by id")
    @ApiResponse(responseCode = "204",description = "User was deleted successfully")
    @ApiResponse(responseCode = "404",description = "User not found")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        try{
            service.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/me")
    @Operation(summary = "Get information about logged user")
    @ApiResponse(responseCode = "200",description = "User was found successfully", content = @Content(schema = @Schema(implementation = UserDTO.class)))
    @ApiResponse(responseCode = "404",description = "User not found", content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject(value = "User not found")))
    public ResponseEntity<UserDTO> readMyself(Principal principal) {
        try {
            return ResponseEntity.ok(mapper.toDTO(service.findByEmail(principal.getName()).get()));
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, e.getMessage(), e
            );
        }
    }

    @GetMapping("/validates")
    @Operation(summary = "Check if token is valid or network is reachable")
    @ApiResponse(responseCode = "200", description = "Token is valid and network is reachable")
    @ApiResponse(responseCode = "403", description = "Token is expired or is not valid")
    public ResponseEntity<?> check() {
        return ResponseEntity.ok().build();
    }

    @GetMapping("/show")
    @Operation(summary = "Show all users with the main information")
    @ApiResponse(responseCode = "200", description = "Users were found successfully", content = @Content(schema = @Schema(implementation = UserDTO.class)))
    public ResponseEntity<?> showAll() {
        return ResponseEntity.ok().body(mapper.toReadAllUsersResponse(service.readAll()));
    }
}
