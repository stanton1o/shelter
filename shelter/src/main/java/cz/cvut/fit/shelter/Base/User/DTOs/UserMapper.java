package cz.cvut.fit.shelter.Base.User.DTOs;

import cz.cvut.fit.shelter.Base.Document.DTOs.DocumentMapper;
import cz.cvut.fit.shelter.Base.User.User;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;



@Mapper(componentModel = "spring", uses = {DocumentMapper.class})
public interface UserMapper extends SuperMapper<User, UserDTO> {

    ReadAllUsersResponse toReadAllUsersResponse(User user);

    Iterable<ReadAllUsersResponse> toReadAllUsersResponse(Iterable<User> users);
}
