package cz.cvut.fit.shelter.Base.Animal.DTOs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "ReadAllAnimalsResponse", description = "Response for showing all animals")
public class ReadAllAnimalsResponse {

    @Schema(description = "Unique identifier of the animal", example = "1", required = true)
    private Long id;
    @Schema(description = "Room id where animal is", example = "32", required = true)
    private Long idRoom;
    @Schema(description = "Name of the animal", example = "Rex", required = true)
    private String name;
    @Schema(description = "Species of the animal", example = "Dog", required = true)
    private String species;
    @Schema(description = "Description of the animal", example = "Dog")
    private String description;

}
