package cz.cvut.fit.shelter.Base.Room.Enclosure;

import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.Room.Room;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Enclosure extends Room {

    @OneToMany(mappedBy = "enclosure", fetch = FetchType.LAZY)
    private List<Animal> animals = new ArrayList<>();

}
