package cz.cvut.fit.shelter.Base.Item.DTOs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "ReadAllItemsResponse", description = "Response for showing all items")
public class ReadAllItemsResponse {
    @Schema(description = "Unique identifier of the item", example = "1", required = true)
    private Long id;
    @Schema(description = "Name of the item", example = "Table", required = true)
    private String name;
    @Schema(description = "Description of the item", example = "Table for living room")
    private String description;
    @Schema(description = "Room id where item is", example = "32", required = true)
    private Long idRoom;
}
