package cz.cvut.fit.shelter.Base.Animal;

public enum Status {
    ADOPTED,
    AVAILABLE,
    SHELTERED,
    ARRIVED,
    IN_REHABILITATION,
    IN_TRAINING,
    LOST,
    RESERVED,
    SICK,
    TRANSFERRED
}
