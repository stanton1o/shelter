package cz.cvut.fit.shelter.Base.Document.DTOs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@Schema(name = "DocumentDTO", description = "Data transfer object for document which is attached to animal")
public class DocumentDTO  {
    @Schema(description = "Unique identifier of the document", example = "1", required = true)
    private Long idDocument;
    @Schema(description = "Name of the document", example = "Document", required = true)
    private String name;
    @Schema(description = "Description of the document", example = "Document for animal", required = true)
    private String contentType;
//    @Schema(description = "Path to the document", example = "C:/Users/Document.pdf", required = true)
//    private String path;
    @Schema(description = "Size of the document", example = "1000", required = true)
    private Long size;
    @Schema(description = "Animal which document is attached to", example = "1", required = true)
    private Long animal;
    @Schema(description = "Creator of the document", example = "1", required = true)
    private Long creator;
    @Schema(description = "Date of creation of the document", example = "2021-05-05T12:00:00", required = true)
    private LocalDateTime dateOfCreation;

    @Schema(hidden = true)
    public Long getId() {
        return idDocument;
    }
    @Schema(hidden = true)
    public void setId(Long id) {
        this.idDocument = id;
    }
}
