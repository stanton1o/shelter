package cz.cvut.fit.shelter.Base.Room.DTOs;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Schema(name = "RoomDTO", description = "Data transfer object for room")
public class RoomDTO implements DataID<Long> {
    @Schema(description = "Unique identifier of the room", example = "1", required = true)
    private Long idRoom;
    @Schema(description = "Name of the room", example = "Living room", required = true)
    private String name;
    @Schema(description = "Description of the room", example = "Room for living")
    private String description;
    @Schema(description = "Area of the room", example = "20.0", required = true)
    private Float area;
    @Schema(description = "Height of the room", example = "3.0", required = true)
    private Float high;
    @Schema(description = "Items which are in the room", example = "[{\"idItem\":1,\"name\":\"Table\",\"description\":\"Table for living room\",\"idRoom\":32}]")
    private List<ItemDTO> items;
    @Schema(description = "Time of room update", example = "2021-12-31T23:59:59.999999999")
    private LocalDateTime timeOfUpdated;
    @Override
    @Schema(hidden = true)
    public Long getId() {
        return idRoom;
    }
    @Override
    @Schema(hidden = true)
    public void setId(Long id) {
        this.idRoom = id;
    }
}
