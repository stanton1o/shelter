package cz.cvut.fit.shelter.Base;

public interface SuperMapper <E, D>{
    D toDTO(E entity);
    E toEntity(D dto);

    Iterable<D> toDTO(Iterable<E> entities);

    Iterable<E> toEntity(Iterable<D> dtos);
}
