package cz.cvut.fit.shelter.Base.Room.DTOs;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalMapper;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemMapper;
import cz.cvut.fit.shelter.Base.Room.Enclosure.Enclosure;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {ItemMapper.class, AnimalMapper.class})
public interface EnclosureMapper extends SuperMapper<Enclosure, EnclosureDTO> {
    @Override
    EnclosureDTO toDTO(Enclosure enclosure);
    @Override
    Enclosure toEntity(EnclosureDTO enclosureDTO);

    @Override
    Iterable<EnclosureDTO> toDTO(Iterable<Enclosure> enclosures);

    @Override
    Iterable<Enclosure> toEntity(Iterable<EnclosureDTO> enclosureDTOs);

    ReadAllEnclosuresResponse toReadAllEnclosuresResponse(Enclosure enclosure);

    Iterable<ReadAllEnclosuresResponse> toReadAllEnclosuresResponse(Iterable<Enclosure> enclosures);
}
