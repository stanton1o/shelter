package cz.cvut.fit.shelter.Base;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

public abstract class CrudController<
        T extends DataID<ID>, ID,
        S extends CrudService<T, ID, R>,
        R extends CrudRepository<T, ID>,
        M extends SuperMapper<T, D>,
        D extends DataID<ID>
        > {
    protected final S service;
    protected final M mapper;

    public CrudController(S service, M mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    @ResponseBody
    @Operation(summary = "Create new entity")
    @ApiResponse(responseCode = "201", description = "Entity was created successfully", content = @Content(schema = @Schema(implementation = DataID.class)))
    @ApiResponse(responseCode = "409", description = "Entity was not created", content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject(value = "Entity was not created")))
    public ResponseEntity<?> create(
            @RequestBody D data) {
        try {
//            D dto = mapper.toDTO(service.create(mapper.toEntity(data)));
//            URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
//                    .buildAndExpand(dto.getId()).toUri();
//            return ResponseEntity.created(URI.create(uri.toString())).build();
            service.create(mapper.toEntity(data));
            return ResponseEntity.status(HttpStatus.CREATED).build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @GetMapping
    @Operation(summary = "Read all entities")
    //content is List of DataID
    @ApiResponse(responseCode = "200", description = "Entities were read successfully", content = @Content(schema = @Schema(implementation = DataID.class)))
    public ResponseEntity<Iterable<D>> readAll() {
        // from all elements in Iterable<T> create Iterable<D>
        return ResponseEntity.ok(mapper.toDTO(service.readAll()));
    }


    @GetMapping("/{id}")
    @ResponseBody
    @Operation(summary = "Read entity by id")
    @ApiResponse(responseCode = "200", description = "Entity was read successfully", content = @Content(schema = @Schema(implementation = DataID.class), examples = @ExampleObject(value = "Entity was read successfully")))
    @ApiResponse(responseCode = "404", description = "Entity not found",content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject(value = "Entity not found")))
    public ResponseEntity<?> readById(@PathVariable ID id) {
        var object = service.readById(id);
        if (object.isPresent())
            return ResponseEntity.ok(mapper.toDTO(object.get()));
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update entity by id")
    @ApiResponse(responseCode = "204", description = "Entity was updated successfully")
    @ApiResponse(responseCode = "404", description = "Entity not found",content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject(value = "Entity not found")))
    public ResponseEntity<?> update(@PathVariable ID id, @RequestBody D data) {
        try {
            service.update(id, mapper.toEntity(data));
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete entity by id")
    @ApiResponse(responseCode = "204", description = "Entity was deleted successfully")
    @ApiResponse(responseCode = "404", description = "Entity not found", content = @Content(schema = @Schema(implementation = String.class), examples = @ExampleObject(value = "Entity not found")))
    public ResponseEntity<?> deleteById(@PathVariable ID id) {
        try{
            service.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
