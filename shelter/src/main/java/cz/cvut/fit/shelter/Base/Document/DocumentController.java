package cz.cvut.fit.shelter.Base.Document;

import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.ByteArrayInputStream;

@RestController
@AllArgsConstructor
@RequestMapping(Constants.CURRENT_API_VERSION +"/documents")
@Tag(name = "DocumentController", description = "API which provides operations for document")
public class DocumentController {

    private final DocumentService documentService;

    @GetMapping("/{id}")
    @Operation(summary = "Download document by id")
    @ApiResponse(responseCode = "200",description = "Document was downloaded successfully")
    @ApiResponse(responseCode = "404",description = "Document not found")
    public ResponseEntity<?> downloadFile(@PathVariable Long id) {
        try{
            Document document = documentService.findById(id);
            ByteArrayInputStream bis = new ByteArrayInputStream(document.getFile());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + document.getName());
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(bis));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete document by id")
    @ApiResponse(responseCode = "204",description = "Document was deleted successfully")
    @ApiResponse(responseCode = "404",description = "Document not found")
    public ResponseEntity<?> deleteFile(@PathVariable Long id) {
        try{
            documentService.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
