package cz.cvut.fit.shelter.Base.Room;

import cz.cvut.fit.shelter.Base.CrudController;
import cz.cvut.fit.shelter.Base.Room.DTOs.RoomDTO;
import cz.cvut.fit.shelter.Base.Room.DTOs.RoomMapper;
import cz.cvut.fit.shelter.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.CURRENT_API_VERSION + "/rooms")
@Tag(name = "RoomController", description = "API which provides operations for room")
public class RoomController extends CrudController<Room, Long, RoomService, RoomRepository, RoomMapper, RoomDTO> {
    public RoomController(RoomService service, RoomMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/show")
    @Operation(summary = "Show all rooms with the main information")
    @ApiResponse(responseCode = "200", description = "Rooms were found successfully", content = @Content(schema = @Schema(implementation = RoomDTO.class)))
    public ResponseEntity<?> showAll() {
        return ResponseEntity.ok().body(mapper.toReadAllRoomsResponse(service.readAll()));
    }
}
