package cz.cvut.fit.shelter.Base.User.DTOs;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Document.DTOs.DocumentDTO;
import cz.cvut.fit.shelter.Base.User.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Getter
@Setter
@Schema(name = "UserDTO", description = "Data transfer object for user")
public class UserDTO implements DataID<Long> {
    @Schema(description = "Unique identifier of the user", example = "1", required = true)
    @NotNull
    private Long idUser;
    @Schema(description = "Name of the user", example = "John", required = true)
    @NotBlank
    private String name;
    @Schema(description = "Surname of the user", example = "Doe", required = true)
    @NotBlank
    private String surname;
    @Schema(description = "Email and login of the user", example = "example@gmail.com", required = true)
    @NotBlank
    private String email;
    @Schema(description = "Password of the user", example = "password", required = true)
    @NotBlank
    private String password;
    @Schema(description = "Phone number of the user", example = "+420 123 456 789")
    private String phone;
    @Schema(description = "Salary of the user", example = "1000.0")
    private Float salary;
    @Schema(description = "Documents which was created by the user")
    private List<DocumentDTO> documents;
    @Schema(description = "Roles of the user", example = "[\"USER\"]", required = true)
    private List<Role> roles;

    @Schema(description = "Time of user update", example = "2021-12-31T23:59:59.999999999")
    private LocalDateTime timeOfUpdated;
    @Schema(description = "User is active", example = "true", required = true)
    private boolean active;
    @Override
    @Schema(hidden = true)
    public Long getId() {
        return idUser;
    }
    @Override
    @Schema(hidden = true)
    public void setId(Long id) {
        this.idUser = id;
    }
}
