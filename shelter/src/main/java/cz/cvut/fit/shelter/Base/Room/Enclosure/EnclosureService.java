package cz.cvut.fit.shelter.Base.Room.Enclosure;

import cz.cvut.fit.shelter.Base.CrudService;
import org.springframework.stereotype.Service;

@Service
public class EnclosureService extends CrudService<Enclosure, Long, EnclosureRepository> {
    public EnclosureService(EnclosureRepository repository) {
        super(repository);
    }
}
