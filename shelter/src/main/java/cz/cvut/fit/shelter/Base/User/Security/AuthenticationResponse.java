package cz.cvut.fit.shelter.Base.User.Security;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthenticationResponse", description = "Response with token")
public class AuthenticationResponse {
    @Schema(description = "Token", example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0QGdtYWlsLmNvbSIsImV4cCI6MTYyNjIwNjQwMCwiaWF0IjoxNjI2MjA2NDAwfQ.1")
    private String token;
}
