package cz.cvut.fit.shelter.Base.Item;

import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Room.Room;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Item implements DataID<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idItem;

    private String name;

    private String description;

    @ManyToOne
    @NotNull
    private Room room;

    private LocalDateTime timeOfUpdated;

    @Override
    public Long getId() {
        return idItem;
    }
    @Override
    public void setId(Long id) {
        this.idItem = id;
    }

}
