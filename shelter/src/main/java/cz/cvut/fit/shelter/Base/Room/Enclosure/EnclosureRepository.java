package cz.cvut.fit.shelter.Base.Room.Enclosure;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnclosureRepository extends CrudRepository<Enclosure, Long> {
}
