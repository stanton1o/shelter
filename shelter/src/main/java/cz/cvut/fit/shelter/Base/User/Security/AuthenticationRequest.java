package cz.cvut.fit.shelter.Base.User.Security;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthenticationRequest", description = "Request for authentication")
public class AuthenticationRequest {

    @NotBlank
    @Schema(description = "Email of the user", example = "example@gmail.com", required = true)
    private String email;
    @NotBlank
    @Schema(description = "Password of the user", example = "password", required = true)
    private String password;
}
