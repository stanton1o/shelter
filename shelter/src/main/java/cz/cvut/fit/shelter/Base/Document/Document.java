package cz.cvut.fit.shelter.Base.Document;

import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.User.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Document  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDocument;
    private String name;
    private String contentType;
    private Long size;
    @Lob
    private byte[] file;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private Animal animal;
    @ManyToOne(cascade = CascadeType.REFRESH)
    private User creator;
    private LocalDateTime dateOfCreation;

    @PrePersist
    void init() {
        this.dateOfCreation = LocalDateTime.now();
    }

    public Long getId() {
        return idDocument;
    }

    public void setId(Long id) {
        this.idDocument = id;
    }
}
