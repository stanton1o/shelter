package cz.cvut.fit.shelter.Base.User.Security;

import cz.cvut.fit.shelter.Base.User.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "RegisterRequest", description = "Request for registration")
public class RegisterRequest {

    @NotBlank
    @Schema(description = "Name of the user", example = "John", required = true)
    private String name;
    @NotBlank
    @Schema(description = "Surname of the user", example = "Doe", required = true)
    private String surname;
    @NotBlank
    @Schema(description = "Email and login of the user", example = "example@gmail.com", required = true)
    private String email;
    @NotBlank
    @Schema(description = "Password of the user", example = "password", required = true)
    private String password;
    @Schema(description = "Roles of the user", example = "[\"USER\"]")
    private Set<Role> roles;
}
