package cz.cvut.fit.shelter.Base.Animal.DTOs;
import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.Document.DTOs.DocumentMapper;
import cz.cvut.fit.shelter.Base.Room.Enclosure.Enclosure;
import cz.cvut.fit.shelter.Base.SuperMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {DocumentMapper.class})
public interface AnimalMapper extends SuperMapper<Animal, AnimalDTO> {
    @Override
    @Mapping(source = "enclosure.idRoom", target = "idRoom")
    @Mapping(source = "documents", target = "documents")
    AnimalDTO toDTO(Animal animal);
    @Override
    @Mapping(source = "idRoom", target = "enclosure")
    Animal toEntity(AnimalDTO animalDTO);

    @Override
    Iterable<AnimalDTO> toDTO(Iterable<Animal> animals);

    @Override
    Iterable<Animal> toEntity(Iterable<AnimalDTO> animalDTOs);

    default Enclosure toEnclosure(Long idRoom){
        Enclosure enclosure = new Enclosure();
        enclosure.setId(idRoom);
        return enclosure;
    }
    @Mapping(source = "enclosure.idRoom", target = "idRoom")
    ReadAllAnimalsResponse toReadAllAnimalsResponse(Animal animal);

    Iterable<ReadAllAnimalsResponse> toReadAllAnimalsResponse(Iterable<Animal> animals);

}
