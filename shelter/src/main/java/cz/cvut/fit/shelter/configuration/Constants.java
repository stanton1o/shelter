package cz.cvut.fit.shelter.configuration;

public class Constants {
    public static final String SUPER_USER_EMAIL = "ADMIN@ADMIN";
    public static final String SUPER_USER_NAME = "ADMIN";
    public static final String SUPER_USER_SURNAME = "ADMIN";
    public static final String SUPER_USER_PASSWORD = "ADMIN";
    public static final String CURRENT_API_VERSION = "/api/v1";
}
