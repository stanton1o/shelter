package cz.cvut.fit.shelter.Base.Room;

import cz.cvut.fit.shelter.Base.CrudService;
import org.springframework.stereotype.Service;

@Service
public class RoomService extends CrudService<Room, Long, RoomRepository> {
    public RoomService(RoomRepository repository) {
        super(repository);
    }
}
