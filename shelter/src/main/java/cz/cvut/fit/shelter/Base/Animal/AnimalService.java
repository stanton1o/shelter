package cz.cvut.fit.shelter.Base.Animal;
import cz.cvut.fit.shelter.Base.CrudService;
import org.springframework.stereotype.Service;


@Service
public class AnimalService extends CrudService<Animal, Long, AnimalRepository> {
    public AnimalService(AnimalRepository repository) {
        super(repository);
    }

}
