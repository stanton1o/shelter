package cz.cvut.fit.shelter.Base.Room.DTOs;

import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalDTO;
import cz.cvut.fit.shelter.Base.DataID;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Schema(name = "EnclosureDTO", description = "Data transfer object for enclosure where animals are kept")
public class EnclosureDTO implements DataID<Long> {
    @Schema(description = "Unique identifier of the enclosure", example = "1", required = true)
    private Long idRoom;
    @Schema(description = "Name of the enclosure", example = "Lion enclosure", required = true)
    private String name;
    @Schema(description = "Description of the enclosure", example = "Enclosure for lions")
    private String description;
    @Schema(description = "Area of the enclosure", example = "200.0")
    private Float area;
    @Schema(description = "Height of the enclosure", example = "5.0")
    private Float high;
    @Schema(description = "Items which are in the enclosure", example = "[{\"idItem\":1,\"name\":\"Table\",\"description\":\"Table for living room\",\"idRoom\":32}]")
    private List<ItemDTO> items;
    @Schema(description = "Animals which are in the enclosure", example = "[{\"idAnimal\":1,\"idRoom\":32,\"name\":\"Rex\",\"species\":\"Dog\",\"breed\":\"Labrador\",\"birthDate\":\"2020-01-01\",\"arrivalDate\":\"2020-01-01\",\"sex\":\"Male\",\"description\":\"Dog\",\"weight\":20.0,\"status\":\"AVAILABLE\",\"documents\":[{\"idDocument\":1,\"name\":\"Document\",\"contentType\":\"Document for animal\",\"path\":\"C:/Users/Document.pdf\",\"size\":1000,\"animal\":1,\"creator\":1}]}]")
    private List<AnimalDTO> animals;
    @Schema(description = "Time of enclosure update", example = "2021-12-31T23:59:59.999999999")
    private LocalDateTime timeOfUpdated;

    @Override
    @Schema(hidden = true)
    public Long getId() {
        return idRoom;
    }
    @Override
    @Schema(hidden = true)
    public void setId(Long id) {
        this.idRoom = id;
    }
}
