package cz.cvut.fit.shelter.Base.Animal;

import cz.cvut.fit.shelter.Base.Room.Enclosure.Enclosure;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AnimalServiceTest {

    @Mock
    AnimalRepository repository;

    @InjectMocks
    AnimalService service;

    @Test
    void testCreate() {
        Animal animal = getAnimal();
        Mockito.when(repository.save(animal)).thenReturn(animal);

        Animal created = service.create(animal);
        assertNotNull(created);
        assertEquals(animal, created);
        Mockito.verify(repository, Mockito.times(1)).save(animal);

        Mockito.when(repository.existsById(animal.getId())).thenReturn(true);
        assertThrows(Exception.class, () -> service.create(animal));
    }

    @Test
    void testReadAll() {
        List<Animal> animals = new ArrayList<>();
        animals.add(getAnimal());
        Animal animal2 = getAnimal();
        animal2.setIdAnimal(2L);
        animal2.setName("KitKAt");
        animals.add(animal2);
        Mockito.when(repository.findAll()).thenReturn(animals);

        Iterable<Animal> all = service.readAll();

        assertNotNull(all);
        assertEquals(2, ((List<Animal>) all).size());
        assertEquals(animals, all);
        assertEquals(animals.get(0), ((List<Animal>) all).get(0));
        assertEquals(animals.get(1), ((List<Animal>) all).get(1));
        assertNotEquals(animals.get(0), ((List<Animal>) all).get(1));
        Mockito.verify(repository, Mockito.times(1)).findAll();

    }

    @Test
    void testReadById() {
        Animal animal = getAnimal();
        Mockito.when(repository.findById(animal.getIdAnimal())).thenReturn(java.util.Optional.of(animal));

        var found = service.readById(1L);

        assert (found.isPresent());
        assertEquals(animal, found.get());
        Mockito.verify(repository, Mockito.times(1)).findById(animal.getIdAnimal());

        found = service.readById(2L);
        assert (found.isEmpty());
    }

    @Test
    void testUpdate() {
        Animal oldAnimal = getAnimal();
        Animal newAnimal = getAnimal();
        newAnimal.setName("I am a new animal");
        Mockito.when(repository.existsById(oldAnimal.getIdAnimal())).thenReturn(true);
        Mockito.when(repository.save(newAnimal)).thenReturn(newAnimal);
        Mockito.when(repository.findById(oldAnimal.getIdAnimal())).thenReturn(java.util.Optional.of(oldAnimal));

        var toUpdate = service.readById(oldAnimal.getIdAnimal());
        assert (toUpdate.isPresent());
        assertEquals(oldAnimal, toUpdate.get());

        Mockito.when(repository.findById(newAnimal.getIdAnimal())).thenReturn(java.util.Optional.of(newAnimal));
        service.update(oldAnimal.getIdAnimal(), newAnimal);

        var updated = service.readById(oldAnimal.getIdAnimal());
        assert (updated.isPresent());
        assertEquals(newAnimal, updated.get());
        assertNotEquals(toUpdate.get(), updated.get());

        Mockito.verify(repository, Mockito.times(1)).save(newAnimal);

        Mockito.when(repository.existsById(404L)).thenReturn(false);
        assertThrows(Exception.class, () -> service.update(404L, newAnimal));
    }

    @Test
    void testDeleteById() {
        Animal animal = getAnimal();
        Mockito.when(repository.existsById(animal.getIdAnimal())).thenReturn(true);
        Mockito.when(repository.findById(animal.getIdAnimal())).thenReturn(java.util.Optional.of(animal));

        var deleted = repository.findById(animal.getIdAnimal());
        assert (deleted.isPresent());
        assertEquals(animal, deleted.get());
        service.deleteById(animal.getIdAnimal());
        Mockito.verify(repository, Mockito.times(1)).deleteById(animal.getIdAnimal());
        Mockito.when(repository.findById(animal.getIdAnimal())).thenReturn(java.util.Optional.empty());
        deleted = repository.findById(animal.getIdAnimal());
        assert (deleted.isEmpty());
        Mockito.when(repository.existsById(animal.getIdAnimal())).thenReturn(false);
        assertThrows(Exception.class, () -> service.deleteById(animal.getIdAnimal()));

    }

    private Animal getAnimal() {
        return Animal.builder()
                .idAnimal(1L)
                .name("Dog")
                .species("Canis lupus familiaris")
                .breed("German Shepherd")
                .birthDate(LocalDateTime.now())
                .arrivalDate(LocalDateTime.now())
                .enclosure(new Enclosure())
                .build();
    }

}