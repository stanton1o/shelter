package cz.cvut.fit.shelter.Base.User;

import cz.cvut.fit.shelter.Base.User.Security.AuthenticationRequest;
import cz.cvut.fit.shelter.Base.User.Security.RegisterRequest;
import cz.cvut.fit.shelter.configuration.Constants;
import cz.cvut.fit.shelter.configuration.JwtService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository repository;

    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private JwtService jwtService;

    @Mock
    private AuthenticationManager authManager;
    @InjectMocks
    private UserService service;

    @Test
    void create() {
        var entity = getEntity();
        Mockito.when(repository.save(entity)).thenReturn(entity);

        var created = service.create(entity);
        assertNotNull(created);
        assertEquals(entity, created);
        Mockito.verify(repository, Mockito.times(1)).save(entity);

        Mockito.when(repository.existsById(entity.getId())).thenReturn(true);
        assertThrows(Exception.class, () -> service.create(entity));
    }

    @Test
    void readAll() {
        List<User> entities = new ArrayList<>();
        entities.add(getEntity());
        var entity2 = getEntity();
        entity2.setId(2L);
        entity2.setName("KitKAt");
        entities.add(entity2);
        Mockito.when(repository.findAll()).thenReturn(entities);

        Iterable<User> all = service.readAll();

        assertNotNull(all);
        assertEquals(2, ((List<User>) all).size());
        assertEquals(entities, all);
        assertEquals(entities.get(0), ((List<User>) all).get(0));
        assertEquals(entities.get(1), ((List<User>) all).get(1));
        assertNotEquals(entities.get(0), ((List<User>) all).get(1));
        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

    @Test
    void readById() {
        var entity = getEntity();
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.of(entity));

        var found = service.readById(1L);

        assert (found.isPresent());
        assertEquals(entity, found.get());
        Mockito.verify(repository, Mockito.times(1)).findById(entity.getId());

        found = service.readById(2L);
        assert (found.isEmpty());
    }

    @Test
    void update() {
        var oldEntity = getEntity();
        var newEntity = getEntity();
        newEntity.setName("I am a new entity");
        Mockito.when(repository.existsById(oldEntity.getId())).thenReturn(true);
        Mockito.when(repository.save(newEntity)).thenReturn(newEntity);
        Mockito.when(repository.findById(oldEntity.getId())).thenReturn(Optional.of(oldEntity));

        /**
         * USer exists and is updated
         */
        var toUpdate = service.readById(oldEntity.getId());
        assert (toUpdate.isPresent());
        assertEquals(oldEntity, toUpdate.get());
        Mockito.when(repository.findById(newEntity.getId())).thenReturn(Optional.of(newEntity));
        service.update(oldEntity.getId(), newEntity);
        var updated = service.readById(oldEntity.getId());
        assert (updated.isPresent());
        assertEquals(newEntity, updated.get());
        assertNotEquals(toUpdate.get(), updated.get());

        /**
         * Test for updating non-existing entity
         */
        Mockito.verify(repository, Mockito.times(1)).save(newEntity);
        Mockito.when(repository.existsById(404L)).thenReturn(false);
        assertThrows(Exception.class, () -> service.update(404L, newEntity));
    }

    @Test
    void deleteById() {
        var entity = getEntity();
        Mockito.when(repository.existsById(entity.getId())).thenReturn(true);
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.of(entity));

        /**
         * Test for deleting existing entity
         */
        var deleted = repository.findById(entity.getId());
        assert (deleted.isPresent());
        assertEquals(entity, deleted.get());
        service.deleteById(entity.getId());
        Mockito.verify(repository, Mockito.times(1)).deleteById(entity.getId());
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.empty());

        /**
         * Test for deleting non-existing entity
         */
        deleted = repository.findById(entity.getId());
        assert (deleted.isEmpty());
        Mockito.when(repository.existsById(entity.getId())).thenReturn(false);
        assertThrows(Exception.class, () -> service.deleteById(entity.getId()));

    }


    @Test
    void findByEmail() {
        var user = getEntity();
        Mockito.when(repository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));

        var found = service.findByEmail(user.getEmail());

        assert (found.isPresent());
        assertEquals(user, found.get());
        Mockito.verify(repository, Mockito.times(1)).findByEmail(user.getEmail());

        found = service.findByEmail("123");
        assert (found.isEmpty());
    }

    @Test
    void register() {
        var request = getRegisterRequest();
        var userEncoded = getEntity();
        userEncoded.setPassword(request.getPassword() + "encoded");

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        Mockito.when(passwordEncoder.encode(request.getPassword())).thenReturn(request.getPassword() + "encoded");
        Mockito.when(repository.save(Mockito.any(User.class))).thenReturn(userEncoded);
        Mockito.when(jwtService.genToken(Mockito.any(User.class))).thenReturn("token");

        /**
         * Test for registering new user
         */
        var response = service.register(request);

        assertNotNull(response);
        assertEquals("token", response.getToken());
        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(passwordEncoder, Mockito.times(1)).encode(request.getPassword());
        Mockito.verify(repository, Mockito.times(1)).save(Mockito.any(User.class));
        Mockito.verify(jwtService, Mockito.times(1)).genToken(Mockito.any(User.class));


        /**
         * Test for user which already exists
         */
        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(userEncoded));
        assertThrows(Exception.class, () -> service.register(request));
        var registered = service.findByEmail(request.getEmail());
        assert (registered.isPresent());
        assertEquals(userEncoded, registered.get());
    }

    @Test
    void authenticate() {
        var request = getAuthenticationRequest();
        var user = getEntity();
        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        assertThrows(Exception.class, () -> service.authenticate(request));

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(user));
        Mockito.when(jwtService.genToken(user)).thenReturn("token");
        var response = service.authenticate(request);
        assertNotNull(response);
        assertEquals("token", response.getToken());
        Mockito.verify(repository, Mockito.times(2)).findByEmail(request.getEmail());
        Mockito.verify(jwtService, Mockito.times(1)).genToken(user);

    }

    /**
     * Test for update method
     */
    @Test
    void testUpdate() {
        var user = getEntity();
        var newUser = getEntity();
        newUser.setName("I am a new user");

        /**
         * Test for user which does not exist
         */
        Mockito.when(repository.existsById(user.getId())).thenReturn(false);
        assertThrows(Exception.class, () -> service.update(user.getId(), newUser));
        Mockito.verify(repository, Mockito.times(1)).existsById(user.getId());

        /**
         * Test for user which exists
         * Password is not changed -> password is not encoded
         */
        Mockito.when(repository.existsById(user.getId())).thenReturn(true);
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(user));
        service.update(user.getId(), newUser);
        Mockito.verify(repository, Mockito.times(2)).existsById(user.getId());
        Mockito.verify(repository, Mockito.times(1)).findById(user.getId());
        Mockito.verify(repository, Mockito.times(1)).save(newUser);
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(newUser));
        var updated = service.readById(user.getId());
        assert (updated.isPresent());
        assertEquals(newUser, updated.get());
        assertNotEquals(user, updated.get());
        assertEquals(user.getPassword(), updated.get().getPassword());

        /**
         * Test for user which exists
         * Password is changed -> password is encoded
         */
        newUser.setPassword("newPassword");
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.encode(newUser.getPassword())).thenReturn(newUser.getPassword() + "encoded");
        service.update(user.getId(), newUser);
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(newUser));
        updated = service.readById(newUser.getId());
        assert (updated.isPresent());
        assertEquals(newUser, updated.get());
        assertNotEquals(user, updated.get());
        assertNotEquals(user.getPassword(), updated.get().getPassword());
        assertNotEquals("newPassword", updated.get().getPassword());
        assertEquals("newPasswordencoded", updated.get().getPassword());
        assertEquals(newUser.getPassword(), updated.get().getPassword());

    }

    private User getEntity() {
        var entity = new User();
        entity.setId(1L);
        entity.setName(Constants.SUPER_USER_NAME);
        entity.setSurname(Constants.SUPER_USER_SURNAME);
        entity.setEmail(Constants.SUPER_USER_EMAIL);
        entity.setPassword(Constants.SUPER_USER_PASSWORD);
        entity.setRoles(EnumSet.of(Role.USER, Role.ADMIN));
        entity.setActive(true);
        entity.setTimeOfUpdated(LocalDateTime.now());
        return entity;
    }

    private RegisterRequest getRegisterRequest() {
        RegisterRequest request = new RegisterRequest();
        request.setName(Constants.SUPER_USER_NAME);
        request.setSurname(Constants.SUPER_USER_SURNAME);
        request.setEmail(Constants.SUPER_USER_EMAIL);
        request.setPassword(Constants.SUPER_USER_PASSWORD);
        request.setRoles(EnumSet.of(Role.USER, Role.ADMIN));
        return request;
    }

    private AuthenticationRequest getAuthenticationRequest() {
        AuthenticationRequest request = new AuthenticationRequest();
        request.setEmail(Constants.SUPER_USER_EMAIL);
        request.setPassword(Constants.SUPER_USER_PASSWORD);
        return request;
    }
}