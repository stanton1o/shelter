package cz.cvut.fit.shelter.Base.User;

import cz.cvut.fit.shelter.configuration.Constants;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;


    @Test
    void findByEmail() {
        var entity = getEntity();
        entity = entityManager.merge(entity);
        /**
         * Test user exists in database
         */
        var result = userRepository.findByEmail(entity.getEmail());
        assertTrue(result.isPresent());
        assertEquals(entity, result.get());

        /**
         * Test user does not exist in database
         */
        result = userRepository.findByEmail("MAIL_DOES_NOT_EXIST");
        assertTrue(result.isEmpty());
    }


    @Test
    void noUserWithRoleExists() {
        var role = Role.ADMIN;
        /**
         * Test no user with role exists
         */
        var result = userRepository.noUserWithRoleExists(role);
        assertTrue(result);

        /**
         * Test user with role exists
         */
        var entity = getEntity();
        entity.setRoles(EnumSet.of(role));
        entityManager.merge(entity);
        result = userRepository.noUserWithRoleExists(role);
        assertFalse(result);

    }

    private User getEntity() {
        var entity = new User();
        entity.setId(1L);
        entity.setName(Constants.SUPER_USER_NAME);
        entity.setSurname(Constants.SUPER_USER_SURNAME);
        entity.setEmail(Constants.SUPER_USER_EMAIL);
        entity.setPassword(Constants.SUPER_USER_PASSWORD);
        entity.setActive(true);
        return entity;
    }
}