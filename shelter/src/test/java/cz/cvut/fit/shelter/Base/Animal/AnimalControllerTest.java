package cz.cvut.fit.shelter.Base.Animal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalDTO;
import cz.cvut.fit.shelter.Base.Animal.DTOs.AnimalMapper;
import cz.cvut.fit.shelter.Base.Animal.DTOs.ReadAllAnimalsResponse;
import cz.cvut.fit.shelter.Base.Room.Enclosure.Enclosure;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class AnimalControllerTest {

    @Mock
    private AnimalService service;
    @Mock
    private AnimalMapper mapper;
    @InjectMocks
    private AnimalController controller;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;
    private final String URL = "/api/v1/animals";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        objectMapper = new ObjectMapper();

    }

    @Test
    void create() throws Exception {
        var dto = getDTO();
        String json = objectMapper.writeValueAsString(dto);
        Mockito.when(mapper.toEntity(getDTO())).thenReturn(getEntity());
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
        Mockito.verify(service, Mockito.times(1)).create(Mockito.any());


        Mockito.when(service.create(Mockito.any())).thenThrow(new RuntimeException());
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isConflict());
        Mockito.verify(service, Mockito.times(2)).create(Mockito.any());

    }

    @Test
    void readAll() throws Exception {
        Iterable<?> collection = getCollectionD();
        Mockito.when(service.readAll()).thenReturn(getCollectionE());
        Mockito.when(mapper.toDTO(service.readAll())).thenReturn(getCollectionD());
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(collection);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readAll();
        var coll = controller.readAll();
        assertNotNull(coll);
        assertEquals(collection.iterator().next(), coll.getBody().iterator().next());

    }

    @Test
    void readById() throws Exception {
        var dto = getDTO();
        var entity = getEntity();
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.empty());

        mockMvc.perform(get(URL + "/{id}", dto.getId()))
                .andExpect(status().isNotFound());
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entity));
        Mockito.when(mapper.toDTO(entity)).thenReturn(dto);

        mockMvc.perform(get(URL + "/{id}", dto.getId()))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(dto);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readById(dto.getId());


    }

    @Test
    void update() throws Exception {
        var dto = getDTO();
        var dtoUpdated = getDTO();
        dtoUpdated.setName("Rex");
        var entity = getEntity();
        var entityUpdated = getEntity();
        entityUpdated.setName("Rex");
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entity));
        Mockito.when(mapper.toDTO(entity)).thenReturn(dto);
        var result = controller.readById(dto.getId());
        assertNotNull(result.getBody());
        assertEquals(dto, result.getBody());

        mockMvc.perform(put(URL + "/{id}", dto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(Objects.requireNonNull(objectMapper.writeValueAsString(dtoUpdated))))
                .andExpect(status().isNoContent());

        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entityUpdated));
        Mockito.when(mapper.toDTO(entityUpdated)).thenReturn(dtoUpdated);
        controller.update(dto.getId(), dtoUpdated);
        result = controller.readById(dto.getId());
        assertNotNull(result.getBody());
        assertEquals(dtoUpdated, result.getBody());
        assertNotEquals(dto, result.getBody());

        Mockito.doThrow(new RuntimeException()).when(service).update(dto.getId(), entityUpdated);
        mockMvc.perform(put(URL + "/{id}", dto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(dtoUpdated))))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteById() throws Exception {
        var dto = getDTO();
        controller.deleteById(dto.getId());
        Mockito.verify(service, Mockito.times(1)).deleteById(dto.getId());

        Mockito.doThrow(new RuntimeException()).when(service).deleteById(dto.getId());
        assertThrows(RuntimeException.class, () -> controller.deleteById(dto.getId()));

        Mockito.doNothing().when(service).deleteById(dto.getId());
        mockMvc.perform(delete(URL + "/{id}", dto.getId()))
                .andExpect(status().isNoContent());

    }

    @Test
    void getStatuses() {
        var res = controller.getStatuses();
        var expected = Arrays.stream(Status.values()).map(Enum::name).toArray(String[]::new);
        assertNotNull(res);
        assertEquals(Status.values().length, res.length);
        assertArrayEquals(expected, res);
    }

    @Test
    void showAll() throws Exception {
        Iterable<?> collection = getCollectionR();
        Mockito.when(service.readAll()).thenReturn(getCollectionE());
        Mockito.when(mapper.toReadAllAnimalsResponse(service.readAll())).thenReturn((Iterable<ReadAllAnimalsResponse>) collection);
        mockMvc.perform(get(URL + "/show"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(collection);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readAll();
        var coll = controller.showAll();
        assertNotNull(coll);

    }

    private Animal getEntity() {
        Animal entity = new Animal();
        var enc = new Enclosure();
        entity.setEnclosure(enc);
        entity.setIdAnimal(1L);
        entity.setId(1L);
        entity.setName("Kitty");
        entity.setSex(Sex.FEMALE);
        entity.setBreed("British");
        entity.setDescription("Cute cat");
        entity.setWeight(2.5F);
        entity.setStatus(Status.AVAILABLE);
        entity.setSpecies("Cat");
        return entity;
    }

    private AnimalDTO getDTO() {
        AnimalDTO dto = new AnimalDTO();
        dto.setIdRoom(1L);
        dto.setId(1L);
        dto.setName("Kitty");
        dto.setSex(Sex.FEMALE);
        dto.setBreed("British");
        dto.setDescription("Cute cat");
        dto.setWeight(2.5F);
        dto.setStatus(Status.AVAILABLE);
        dto.setSpecies("Cat");
        return dto;
    }

    private Iterable<AnimalDTO> getCollectionD() {
        return List.of(getDTO(), getDTO());
    }

    private Iterable<Animal> getCollectionE() {
        return List.of(getEntity(), getEntity());
    }

    Iterable<ReadAllAnimalsResponse> getCollectionR() {
        ReadAllAnimalsResponse response = new ReadAllAnimalsResponse();
        response.setId(1L);
        response.setName("Kitty");
        response.setIdRoom(1L);
        response.setDescription("Cute cat");
        response.setSpecies("Cat");

        return List.of(response, response);
    }
}