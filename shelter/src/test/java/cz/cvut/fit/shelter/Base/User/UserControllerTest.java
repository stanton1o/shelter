package cz.cvut.fit.shelter.Base.User;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.shelter.Base.Document.DocumentService;
import cz.cvut.fit.shelter.Base.User.DTOs.ReadAllUsersResponse;
import cz.cvut.fit.shelter.Base.User.DTOs.UserDTO;
import cz.cvut.fit.shelter.Base.User.DTOs.UserMapper;
import cz.cvut.fit.shelter.Base.User.Security.AuthenticationRequest;
import cz.cvut.fit.shelter.Base.User.Security.AuthenticationResponse;
import cz.cvut.fit.shelter.Base.User.Security.RegisterRequest;
import cz.cvut.fit.shelter.configuration.Constants;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    private final String URL = "/api/v1/staffs";
    @Mock
    private DocumentService serviceDoc;
    @Mock
    private UserService service;

    @Mock
    private UserMapper mapper;
    @InjectMocks
    private UserController controller;

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void readAll() throws Exception {
        Iterable<?> collection = getCollectionD();
        Mockito.when(service.readAll()).thenReturn(getCollectionE());
        Mockito.when(mapper.toDTO(service.readAll())).thenReturn(getCollectionD());
        mockMvc.perform(get(URL))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(collection);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readAll();
        var coll = controller.readAll();
        assertNotNull(coll);
        assertEquals(collection.iterator().next(), coll.getBody().iterator().next());

    }

    @Test
    void readById() throws Exception {
        var dto = getDTO();
        var entity = getEntity();
        /**
         * User not found
         */
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.empty());
        mockMvc.perform(get(URL + "/{id}", dto.getId()))
                .andExpect(status().isNotFound());

        /**
         * User found
         */
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entity));
        Mockito.when(mapper.toDTO(entity)).thenReturn(dto);
        mockMvc.perform(get(URL + "/{id}", dto.getId()))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(dto);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readById(dto.getId());


    }

    @Test
    void update() throws Exception {
        var dto = getDTO();
        var dtoUpdated = getDTO();
        dtoUpdated.setName("Rex");
        var entity = getEntity();
        var entityUpdated = getEntity();
        entityUpdated.setName("Rex");
        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entity));
        Mockito.when(mapper.toDTO(entity)).thenReturn(dto);
        var result = controller.readById(dto.getId());
        assertNotNull(result.getBody());
        assertEquals(dto, result.getBody());
        /**
         * Successful update and check if the object's parameters have changed
         */
        mockMvc.perform(put(URL + "/{id}", dto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(dtoUpdated))))
                .andExpect(status().isNoContent());

        Mockito.when(service.readById(dto.getId())).thenReturn(Optional.of(entityUpdated));
        Mockito.when(mapper.toDTO(entityUpdated)).thenReturn(dtoUpdated);
        controller.update(dto.getId(), dtoUpdated);
        result = controller.readById(dto.getId());
        assertNotNull(result.getBody());
        assertEquals(dtoUpdated, result.getBody());
        assertNotEquals(dto, result.getBody());

        /**
         * User not found and cannot be updated
         */
        Mockito.doThrow(new RuntimeException()).when(service).update(dto.getId(), entityUpdated);
        mockMvc.perform(put(URL + "/{id}", dto.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(dtoUpdated))))
                .andExpect(status().isNotFound());
    }

    @Test
    void uploadFile() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "file",
                "test.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        mockMvc.perform(
                        multipart(URL + "/{idStaff}/documents/{idAnimal}", 1L, 1L)
                                .file(file))
                .andExpect(status().isCreated());

        /**
         * Document was not uploaded. Error during upload
         */
        Mockito.doThrow(new RuntimeException()).when(serviceDoc).upload(file, 1L, 1L);
        mockMvc.perform(
                        multipart(URL + "/{idStaff}/documents/{idAnimal}", 1L, 1L)
                                .file(file))
                .andExpect(status().isConflict());

        Mockito.verify(serviceDoc, Mockito.times(2)).upload(file, 1L, 1L);

    }

    /**
     * This endpoint is deprecated
     */
    @Test
    void create() throws Exception {
        mockMvc.perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(getDTO()))))
                .andExpect(status().isMovedPermanently())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = "Use " + Constants.CURRENT_API_VERSION + "/staffs/register endpoint to create new user";
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
    }

    @Test
    void registerUser() throws Exception {
        var request = getRegisterRequest();
        AuthenticationResponse response = AuthenticationResponse.builder()
                .token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0QGdtYWlsLmNvbSIsImV4cCI6MTYyNjIwNjQwMCwiaWF0IjoxNjI2MjA2NDAwfQ.1").build();

        /**
         * User with email which is reserved
         */
        mockMvc.perform(post(URL + "/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isForbidden());
        /**
         * User with email has been registered and return JWT
         */
        request.setEmail(request.getEmail().replace("ADMIN", "USER"));
        Mockito.when(service.register(request)).thenReturn(response);
        mockMvc.perform(post(URL + "/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(response);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        /**
         * User with email already exists
         */
        Mockito.doThrow(new EntityExistsException("User with email: " + request.getEmail() + " already exists")).when(service).register(request);
        mockMvc.perform(post(URL + "/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isConflict());

        /**
         * Another error
         */
        Mockito.doThrow(new RuntimeException()).when(service).register(request);
        mockMvc.perform(post(URL + "/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isInternalServerError());

        Mockito.verify(service, Mockito.times(3)).register(request);

    }

    @Test
    void loginUser() throws Exception {
        var request = getAuthenticationRequest();
        AuthenticationResponse response = AuthenticationResponse.builder()
                .token("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0QGdtYWlsLmNvbSIsImV4cCI6MTYyNjIwNjQwMCwiaWF0IjoxNjI2MjA2NDAwfQ.1").build();
        var bannedUser = getEntity();
        bannedUser.setActive(false);

        /**
         * User exists and is blocked
         */
        Mockito.when(service.findByEmail(request.getEmail())).thenReturn(Optional.of(bannedUser));
        mockMvc.perform(post(URL + "/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isForbidden());

        /**
         * User exists and write correct password and email
         * Return JWT
         */
        var user = getEntity();
        Mockito.when(service.findByEmail(request.getEmail())).thenReturn(Optional.of(user));
        Mockito.when(service.authenticate(request)).thenReturn(response);
        mockMvc.perform(post(URL + "/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(response);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });

        /**
         * User does not exist
         */
        Mockito.doThrow(new EntityNotFoundException("User with email: " + request.getEmail() + " does not exist")).when(service).authenticate(request);
        mockMvc.perform(post(URL + "/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Objects.requireNonNull(objectMapper.writeValueAsString(request)))
                )
                .andExpect(status().isConflict());

    }


    @Test
    void readMyself() throws Exception {
        var dto = getDTO();
        var entity = getEntity();
        Principal principal = dto::getEmail;

        /**
         * User not found or cannot be authenticated

         */
        Mockito.when(service.findByEmail(dto.getEmail())).thenReturn(Optional.empty());
        assertThrows(ResponseStatusException.class, () -> controller.readMyself(principal).getStatusCode().isSameCodeAs(HttpStatusCode.valueOf(404)));
        mockMvc.perform(get(URL + "/me", principal))
                .andExpect(status().isNotFound());

        /**
         * User authenticated and found
         */
        Mockito.when(service.findByEmail(dto.getEmail())).thenReturn(Optional.of(entity));
        Mockito.when(mapper.toDTO(entity)).thenReturn(dto);
        var res = controller.readMyself(principal);
        assertTrue(res.getStatusCode().isSameCodeAs(HttpStatusCode.valueOf(200)));
        assertEquals(dto, res.getBody());

        Mockito.verify(service, Mockito.times(2)).findByEmail(dto.getEmail());
    }

    /**
     * Test if token is not expired
     */
    @Test
    void check() throws Exception {
        mockMvc.perform(get(URL + "/validates"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteById() throws Exception {
        var dto = getDTO();
        controller.deleteById(dto.getId());
        Mockito.verify(service, Mockito.times(1)).deleteById(dto.getId());

        /**
         * User not found
         */
        Mockito.doThrow(new RuntimeException()).when(service).deleteById(dto.getId());
        assertThrows(ResponseStatusException.class, () -> controller.deleteById(dto.getId()));

        /**
         * User found and deleted
         */
        Mockito.doNothing().when(service).deleteById(dto.getId());
        mockMvc.perform(delete(URL + "/{id}", dto.getId()))
                .andExpect(status().isNoContent());

    }

    /**
     * The same as readAll but ReadAllUsersResponse instead of UserDTO
     */
    @Test
    void showAll() throws Exception {
        Iterable<?> collection = getCollectionR();
        Mockito.when(service.readAll()).thenReturn(getCollectionE());
        Mockito.when(mapper.toReadAllUsersResponse(service.readAll())).thenReturn((Iterable<ReadAllUsersResponse>) collection);
        mockMvc.perform(get(URL + "/show"))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    String json = result.getResponse().getContentAsString();
                    String expected = objectMapper.writeValueAsString(collection);
                    org.junit.jupiter.api.Assertions.assertEquals(expected, json);
                });
        Mockito.verify(service, Mockito.times(2)).readAll();
        var coll = controller.showAll();
        assertNotNull(coll);

    }

    private User getEntity() {
        var entity = new User();
        entity.setId(1L);
        entity.setName(Constants.SUPER_USER_NAME);
        entity.setSurname(Constants.SUPER_USER_SURNAME);
        entity.setEmail(Constants.SUPER_USER_EMAIL);
        entity.setPassword(Constants.SUPER_USER_PASSWORD);
        entity.setActive(true);
        return entity;
    }

    private UserDTO getDTO() {
        var dto = new UserDTO();
        dto.setId(1L);
        dto.setName(Constants.SUPER_USER_NAME);
        dto.setSurname(Constants.SUPER_USER_SURNAME);
        dto.setEmail(Constants.SUPER_USER_EMAIL);
        dto.setPassword(Constants.SUPER_USER_PASSWORD);
        dto.setActive(true);
        return dto;
    }

    private Iterable<UserDTO> getCollectionD() {
        return List.of(getDTO(), getDTO());
    }

    private Iterable<User> getCollectionE() {
        return List.of(getEntity(), getEntity());
    }

    Iterable<ReadAllUsersResponse> getCollectionR() {
        var response = new ReadAllUsersResponse();
        response.setId(1L);
        response.setName(Constants.SUPER_USER_NAME);
        response.setSurname(Constants.SUPER_USER_SURNAME);
        response.setEmail(Constants.SUPER_USER_EMAIL);
        return List.of(response, response);
    }

    private RegisterRequest getRegisterRequest() {
        RegisterRequest request = new RegisterRequest();
        request.setName(Constants.SUPER_USER_NAME);
        request.setSurname(Constants.SUPER_USER_SURNAME);
        request.setEmail(Constants.SUPER_USER_EMAIL);
        request.setPassword(Constants.SUPER_USER_PASSWORD);
        request.setRoles(EnumSet.of(Role.USER, Role.ADMIN));
        return request;
    }

    private AuthenticationRequest getAuthenticationRequest() {
        AuthenticationRequest request = new AuthenticationRequest();
        request.setEmail(Constants.SUPER_USER_EMAIL);
        request.setPassword(Constants.SUPER_USER_PASSWORD);
        return request;
    }
}