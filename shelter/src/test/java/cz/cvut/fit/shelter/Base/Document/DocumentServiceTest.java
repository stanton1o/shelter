package cz.cvut.fit.shelter.Base.Document;

import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.Animal.AnimalService;
import cz.cvut.fit.shelter.Base.User.User;
import cz.cvut.fit.shelter.Base.User.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DocumentServiceTest {

    @Mock
    private  DocumentRepository documentRepository;

    @Mock
    private UserService userService;

    @Mock
    private AnimalService animalService;

    @InjectMocks
    private DocumentService service;


    @Test
    void upload() throws IOException {

        var doc = getEntity();

        MockMultipartFile file = new MockMultipartFile(doc.getName(), "test.txt", doc.getContentType(), doc.getFile());
        Document document = multipartFileToDocument(file);

        Mockito.when(userService.readById(doc.getCreator().getId())).thenReturn(Optional.of(doc.getCreator()));
        Mockito.when(animalService.readById(doc.getAnimal().getIdAnimal())).thenReturn(Optional.of(doc.getAnimal()));

        var staff = userService.readById(doc.getCreator().getId());
        var animal = animalService.readById(doc.getAnimal().getIdAnimal());
        assert (animal.isPresent());
        assert (staff.isPresent());

        document.setCreator(staff.get());
        document.setAnimal(animal.get());

        Mockito.when(documentRepository.findById(doc.getId())).thenReturn(Optional.of(document));
        service.upload(file, doc.getCreator().getId(), doc.getAnimal().getIdAnimal());
        Mockito.verify(documentRepository, Mockito.times(1)).save(Mockito.any(Document.class));

        var result = service.findById(doc.getId());
        assertNotNull(result);
        assertEquals(document, result);
        assertThrows(Exception.class, () -> service.upload(file, 2L, 2L)); //
    }

    @Test
    void findById() {
        var entity = getEntity();
        Mockito.when(documentRepository.findById(entity.getId())).thenReturn(Optional.of(entity));

        var found = service.findById(1L);

        assertNotNull(found);
        assertEquals(entity, found);
        Mockito.verify(documentRepository, Mockito.times(1)).findById(entity.getId());

        assertThrows(Exception.class, () -> service.findById(2L));

    }

    @Test
    void deleteById() {
        var entity = getEntity();
        Mockito.when(documentRepository.findById(entity.getId())).thenReturn(Optional.of(entity));

        var deleted = documentRepository.findById(entity.getId());
        assert (deleted.isPresent());
        assertEquals(entity, deleted.get());
        service.deleteById(entity.getId());
        Mockito.verify(documentRepository, Mockito.times(1)).deleteById(entity.getId());
        Mockito.when(documentRepository.findById(entity.getId())).thenReturn(Optional.empty());
        deleted = documentRepository.findById(entity.getId());
        assert (deleted.isEmpty());
    }

    private Document getEntity() {
        Document entity = new Document();
        entity.setId(1L);
        entity.setName("KitKat");
        entity.setFile(new byte[]{1, 2, 3});
        entity.setContentType("application/pdf");
        entity.setSize(1024L);
        // Mock user
        User staff = new User();
        staff.setId(1L);

        // Mock animal
        Animal animal = new Animal();
        animal.setIdAnimal(1L);

        entity.setAnimal(animal);
        entity.setCreator(staff);

        return entity;
    }

    private Document multipartFileToDocument(MultipartFile file) throws IOException {
        Document document = new Document();
        document.setName(file.getOriginalFilename());
        document.setContentType(file.getContentType());
        document.setSize(file.getSize());
        document.setFile(file.getBytes());
        return document;
    }
}