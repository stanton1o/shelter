package cz.cvut.fit.shelter.Base.Room.Enclosure;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class EnclosureServiceTest {

    @Mock
    EnclosureRepository repository;

    @InjectMocks
    EnclosureService service;

    @Test
    void create() {
        var entity = getEntity();
        Mockito.when(repository.save(entity)).thenReturn(entity);

        var created = service.create(entity);
        assertNotNull(created);
        assertEquals(entity, created);
        Mockito.verify(repository, Mockito.times(1)).save(entity);

        Mockito.when(repository.existsById(entity.getId())).thenReturn(true);
        assertThrows(Exception.class, () -> service.create(entity));
    }

    @Test
    void readAll() {
        List<Enclosure> entities = new ArrayList<>();
        entities.add(getEntity());
        var entity2 = getEntity();
        entity2.setId(2L);
        entity2.setName("KitKAt");
        entities.add(entity2);
        Mockito.when(repository.findAll()).thenReturn(entities);

        Iterable<Enclosure> all = service.readAll();

        assertNotNull(all);
        assertEquals(2, ((List<Enclosure>) all).size());
        assertEquals(entities, all);
        assertEquals(entities.get(0), ((List<Enclosure>) all).get(0));
        assertEquals(entities.get(1), ((List<Enclosure>) all).get(1));
        assertNotEquals(entities.get(0), ((List<Enclosure>) all).get(1));
        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

    @Test
    void readById() {
        var entity = getEntity();
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.of(entity));

        var found = service.readById(1L);

        assert (found.isPresent());
        assertEquals(entity, found.get());
        Mockito.verify(repository, Mockito.times(1)).findById(entity.getId());

        found = service.readById(2L);
        assert (found.isEmpty());
    }

    @Test
    void update() {
        var oldEntity = getEntity();
        var newEntity = getEntity();
        newEntity.setName("I am a new entity");
        Mockito.when(repository.existsById(oldEntity.getId())).thenReturn(true);
        Mockito.when(repository.save(newEntity)).thenReturn(newEntity);
        Mockito.when(repository.findById(oldEntity.getId())).thenReturn(Optional.of(oldEntity));

        var toUpdate = service.readById(oldEntity.getId());
        assert (toUpdate.isPresent());
        assertEquals(oldEntity, toUpdate.get());

        Mockito.when(repository.findById(newEntity.getId())).thenReturn(Optional.of(newEntity));
        service.update(oldEntity.getId(), newEntity);

        var updated = service.readById(oldEntity.getId());
        assert (updated.isPresent());
        assertEquals(newEntity, updated.get());
        assertNotEquals(toUpdate.get(), updated.get());

        Mockito.verify(repository, Mockito.times(1)).save(newEntity);
        Mockito.when(repository.existsById(404L)).thenReturn(false);
        assertThrows(Exception.class, () -> service.update(404L, newEntity));
    }

    @Test
    void deleteById() {
        var entity = getEntity();
        Mockito.when(repository.existsById(entity.getId())).thenReturn(true);
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.of(entity));

        var deleted = repository.findById(entity.getId());
        assert (deleted.isPresent());
        assertEquals(entity, deleted.get());
        service.deleteById(entity.getId());
        Mockito.verify(repository, Mockito.times(1)).deleteById(entity.getId());
        Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.empty());
        deleted = repository.findById(entity.getId());
        assert (deleted.isEmpty());
        Mockito.when(repository.existsById(entity.getId())).thenReturn(false);
        assertThrows(Exception.class, () -> service.deleteById(entity.getId()));

    }


    private Enclosure getEntity() {
        var enclosure = new Enclosure();
        enclosure.setId(1L);
        enclosure.setName("Enclosure");
        enclosure.setAnimals(new ArrayList<>());
        enclosure.setArea(100F);
        enclosure.setHigh(10F);
        enclosure.setItems(new ArrayList<>());
        enclosure.setTimeOfUpdated(LocalDateTime.now());
        return enclosure;
    }
}