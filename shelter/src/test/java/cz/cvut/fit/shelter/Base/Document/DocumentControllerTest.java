package cz.cvut.fit.shelter.Base.Document;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.shelter.Base.Item.DTOs.ItemMapper;
import cz.cvut.fit.shelter.Base.Item.ItemController;
import cz.cvut.fit.shelter.Base.Item.ItemService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)

class DocumentControllerTest {
    @Mock
    private DocumentService service;

    @InjectMocks
    private DocumentController controller;
    private MockMvc mockMvc;
    private final String URL = "/api/v1/documents";

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void downloadFile() throws Exception {

        Document document = new Document();
        document.setId(1L);
        document.setName("name");
        document.setFile(new byte[]{1, 2, 3, 4, 5});
        Mockito.when(service.findById(1L)).thenReturn(document);

        mockMvc.perform(get(URL + "/{id}", 1))
                .andExpect(status().isOk())
                        .andExpect(result -> {
                            byte[] bytes = result.getResponse().getContentAsByteArray();
                            assertArrayEquals(document.getFile(), bytes);
                        });

        Mockito.doThrow(new RuntimeException()).when(service).findById(1L);
        mockMvc.perform(get(URL + "/{id}", 1))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteFile() throws Exception {
        mockMvc.perform(delete(URL + "/{id}", 1))
                .andExpect(status().isNoContent());

        Mockito.verify(service, Mockito.times(1)).deleteById(1L);

        Mockito.doThrow(new RuntimeException()).when(service).deleteById(1L);
        mockMvc.perform(delete(URL + "/{id}", 1))
                .andExpect(status().isNotFound());

    }
}