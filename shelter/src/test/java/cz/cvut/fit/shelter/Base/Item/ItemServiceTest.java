package cz.cvut.fit.shelter.Base.Item;

import cz.cvut.fit.shelter.Base.Animal.Animal;
import cz.cvut.fit.shelter.Base.Room.Room;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ItemServiceTest {

    @Mock
    ItemRepository repository;

    @InjectMocks
    ItemService service;

    @Test
    void create() {
        Item item = getItem();
        Mockito.when(repository.save(item)).thenReturn(item);

        var created = service.create(item);
        assertNotNull(created);
        assertEquals(item, created);
        Mockito.verify(repository, Mockito.times(1)).save(item);

        Mockito.when(repository.existsById(item.getId())).thenReturn(true);
        assertThrows(Exception.class, () -> service.create(item));
    }

    @Test
    void readAll() {
        List<Item> items = new ArrayList<>();
        items.add(getItem());
        Item item2 = getItem();
        item2.setIdItem(2L);
        item2.setName("KitKAt");
        items.add(item2);
        Mockito.when(repository.findAll()).thenReturn(items);

        Iterable<Item> all = service.readAll();

        assertNotNull(all);
        assertEquals(2, ((List<Item>) all).size());
        assertEquals(items, all);
        assertEquals(items.get(0), ((List<Item>) all).get(0));
        assertEquals(items.get(1), ((List<Item>) all).get(1));
        assertNotEquals(items.get(0), ((List<Item>) all).get(1));
        Mockito.verify(repository, Mockito.times(1)).findAll();
    }

    @Test
    void readById() {
        var item = getItem();
        Mockito.when(repository.findById(item.getIdItem())).thenReturn(Optional.of(item));

        var found = service.readById(1L);

        assert (found.isPresent());
        assertEquals(item, found.get());
        Mockito.verify(repository, Mockito.times(1)).findById(item.getIdItem());

        found = service.readById(2L);
        assert (found.isEmpty());
    }

    @Test
    void update() {
        var oldEntity = getItem();
        var newEntity = getItem();
        newEntity.setName("I am a new entity");
        Mockito.when(repository.existsById(oldEntity.getIdItem())).thenReturn(true);
        Mockito.when(repository.save(newEntity)).thenReturn(newEntity);
        Mockito.when(repository.findById(oldEntity.getIdItem())).thenReturn(Optional.of(oldEntity));

        var toUpdate = service.readById(oldEntity.getIdItem());
        assert (toUpdate.isPresent());
        assertEquals(oldEntity, toUpdate.get());

        Mockito.when(repository.findById(newEntity.getIdItem())).thenReturn(Optional.of(newEntity));
        service.update(oldEntity.getIdItem(), newEntity);

        var updated = service.readById(oldEntity.getIdItem());
        assert (updated.isPresent());
        assertEquals(newEntity, updated.get());
        assertNotEquals(toUpdate.get(), updated.get());

        Mockito.verify(repository, Mockito.times(1)).save(newEntity);
        Mockito.when(repository.existsById(404L)).thenReturn(false);
        assertThrows(Exception.class, () -> service.update(404L, newEntity));
    }

    @Test
    void deleteById() {
        var entity = getItem();
        Mockito.when(repository.existsById(entity.getIdItem())).thenReturn(true);
        Mockito.when(repository.findById(entity.getIdItem())).thenReturn(Optional.of(entity));

        var deleted = repository.findById(entity.getIdItem());
        assert (deleted.isPresent());
        assertEquals(entity, deleted.get());
        service.deleteById(entity.getIdItem());
        Mockito.verify(repository, Mockito.times(1)).deleteById(entity.getIdItem());
        Mockito.when(repository.findById(entity.getIdItem())).thenReturn(Optional.empty());
        deleted = repository.findById(entity.getIdItem());
        assert (deleted.isEmpty());
        Mockito.when(repository.existsById(entity.getIdItem())).thenReturn(false);
        assertThrows(Exception.class, () -> service.deleteById(entity.getIdItem()));

    }


    private Item getItem() {
        Item item = new Item();
        item.setIdItem(1L);
        item.setName("Item");
        item.setDescription("Description");
        item.setTimeOfUpdated(LocalDateTime.now());
        item.setRoom(new Room());
        return item;
    }
}