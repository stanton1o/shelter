import { DOCUMENTATION_ROUTE, STAFF_ROUTE } from "../utils/constants";
import { $authHost} from "./index";

export const readStaffById = async (id) => {
    const {data} = await $authHost.get(STAFF_ROUTE +`/${id}`);
    return data;
}

export const readAllStaff = async () => {
    const {data} = await $authHost.get(STAFF_ROUTE );
    return data;
}

export const deleteStaffById = async (id) => {
    await $authHost.delete(STAFF_ROUTE +`/${id}`);
}

export const updateStaffById = async (id, user) => {
    await $authHost.put(STAFF_ROUTE +`/${id}`, user);
}

export const uploadFile = async (file, idStaff, idAnimal) => {
    await $authHost.post(STAFF_ROUTE +`/${idStaff}`+DOCUMENTATION_ROUTE+`/${idAnimal}`, file);
}

