import { $authHost } from ".";
import { ITEM_ROUTE } from "../utils/constants";

export const readAllItems = async () => {
    const {data} = await $authHost.get(ITEM_ROUTE);
    return data;
}

export const readItemById = async (id) => {
    const {data} = await $authHost.get(`${ITEM_ROUTE}/${id}`);
    return data;
}

export const createItem = async (item) => {
    await $authHost.post(ITEM_ROUTE, item);
}

export const updateItem = async (id, item) => {
    await $authHost.put(`${ITEM_ROUTE}/${id}`, item);
}

export const deleteItemById = async (id) => {
    await $authHost.delete(`${ITEM_ROUTE}/${id}`);
}