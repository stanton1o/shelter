import { LOGIN_ROUTE, REGISTER_ROUTE, STAFF_ROUTE } from "../utils/constants";
import { $authHost, $host } from "./index";

import { jwtDecode } from "jwt-decode";

export const login = async (email, password) => {
    const {data} = await $host.post(LOGIN_ROUTE, {email, password});
    localStorage.setItem('token', data.token);
    console.log(data.token);
    console.log(jwtDecode(data.token));
    return jwtDecode(data.token);
}

export const registration = async (name, surname, email, password, roles) => {
    // console.log(name, surname, email, password, roles)
    const {data} = await $authHost.post(REGISTER_ROUTE, {name, surname, email, password, roles});
    // localStorage.setItem('token', data.token);
    return jwtDecode(data.token);
}

export const readMyself = async () => {
    const {data} = await $authHost.get(STAFF_ROUTE + '/me');
    return data;
}

export const showAll = async (location) => {
    const {data} = await $authHost.get(location + '/show');
    return data;
}

export const validate = async () => {
    return await $authHost.get(`${STAFF_ROUTE}/validates`);
}
