import { DOCUMENTATION_ROUTE } from "../utils/constants";
import { $authHost} from "./index";



export const downloadFileById = async (docData) => {
    try {
        const response = await $authHost.get(DOCUMENTATION_ROUTE + `/${docData.idDocument}`, {
            responseType: 'blob' 
        });
        
        const blob = new Blob([response.data], { type: response.headers['content-type'] });
        const downloadUrl = window.URL.createObjectURL(blob)
        const link = document.createElement('a')
        link.href = downloadUrl
        link.download = docData.name
        document.body.appendChild(link)
        link.click()
        link.remove()
    } catch (e) {
        console.error("Error fetching user data:", e);
    }
    
};


export const deleteFileById = async (id) => {
    await $authHost.delete(DOCUMENTATION_ROUTE + `/${id}`);
}