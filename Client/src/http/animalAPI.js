import { $authHost } from "./index";
import { ANIMAL_ROUTE } from "../utils/constants";

export const readAnimalById = async (id) => {
    const {data} = await $authHost.get(ANIMAL_ROUTE +`/${id}`);
    return data;
}

export const deleteAnimalById = async (id) => {
    await $authHost.delete(ANIMAL_ROUTE +`/${id}`);
}

export const updateAnimalById = async (id, animal) => {
    await $authHost.put(ANIMAL_ROUTE +`/${id}`, animal);
}

export const createAnimal = async (animal) => {
    await $authHost.post(ANIMAL_ROUTE, animal);
}

export const readStatuses = async () => {
    const {data} = await $authHost.get(ANIMAL_ROUTE + '/statuses');
    return data;
}