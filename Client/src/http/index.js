import axios from 'axios'
import { SERVER_URL } from '../utils/constants'

const $host = axios.create({
    baseURL: SERVER_URL
})

const $authHost = axios.create({
    baseURL: SERVER_URL
})

const authInterceptor = config =>{
    config.headers.Authorization =  `Bearer ${localStorage.getItem('token')}`
    return config
}

$authHost.interceptors.request.use(authInterceptor)

export{
    $authHost,
    $host
}
