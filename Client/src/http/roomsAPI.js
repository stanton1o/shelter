import { $authHost } from ".";
import { ENCLOSURE_ROUTE, ROOM_ROUTE } from "../utils/constants";

export const readAllRooms = async () => {
    const {data} = await $authHost.get(ROOM_ROUTE);
    return data;
}

export const readAllEnclosures = async () => {
    const {data} = await $authHost.get(ENCLOSURE_ROUTE);
    return data;
}

export const readRoomById = async (id) => {
    const {data} = await $authHost.get(`${ROOM_ROUTE}/${id}`);
    return data;
}

export const readEnclosureById = async (id) => {
    const {data} = await $authHost.get(`${ENCLOSURE_ROUTE}/${id}`);
    return data;
}

export const deleteRoomById = async (id) => {
    await $authHost.delete(`${ROOM_ROUTE}/${id}`);
}

export const createRoom = async (room) => {
    await $authHost.post(ROOM_ROUTE, room);
}

export const createEnclosure = async (enclosure) => {
    await $authHost.post(ENCLOSURE_ROUTE, enclosure);
}

export const updateRoom = async (id, room) => {
    await $authHost.put(`${ROOM_ROUTE}/${id}`, room);
}

export const updateEnclosure = async (id, enclosure) => {
    await $authHost.put(`${ENCLOSURE_ROUTE}/${id}`, enclosure);
}