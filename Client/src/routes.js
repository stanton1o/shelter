import { HOME_ROUTE, ADMIN_ROUTE, ANIMAL_ROUTE, ROOM_ROUTE, ITEM_ROUTE, LOGIN_ROUTE, STAFF_ROUTE } from "./utils/constants" 
import Auth from "./pages/Auth"
import Home from "./pages/Home"
import Admin from "./pages/Admin"
import Animal from "./pages/Animal"
import Room from "./pages/Room"
import Item from "./pages/Item"
import ElementCollection from "./pages/ElementCollection"
import Staff from "./pages/Staff"

export const authRoutes = [
    {
        path: HOME_ROUTE,
        Component: Home
    },
    {
        path: ADMIN_ROUTE,
        Component: Admin
    },
    {
        path: ANIMAL_ROUTE,
        Component: ElementCollection
    
    },
    {
        path: ANIMAL_ROUTE + '/:id',
        Component: Animal
    
    },
    {
        path: ROOM_ROUTE,
        Component: ElementCollection
    },
    {
        path: ROOM_ROUTE + '/:id',
        Component: Room
    },
    {
        path: ITEM_ROUTE,
        Component: ElementCollection
    },
    {
        path: ITEM_ROUTE + '/:id',
        Component: Item
    },
    {
        path: STAFF_ROUTE,
        Component: ElementCollection
    },
    {
        path: STAFF_ROUTE + '/:id',
        Component: Staff
    }
]

export const publicRoutes = [

    {
        path: LOGIN_ROUTE,
        Component: Auth
    }
]