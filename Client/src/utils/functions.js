import { downloadFileById } from "../http/docAPI";

export function replaceValues(targetObject, sourceObject) {
    Object.keys(targetObject).forEach(key => {
        if (sourceObject.hasOwnProperty(key)) {
            targetObject[key] = sourceObject[key];
        }
    });
    return targetObject;
}

export const downloadClickHandler = (e, doc) => {
    e.stopPropagation()
    downloadFileById(doc)
}
