export const SERVER_URL = 'http://localhost:8080/api/v1'

export const HOME_ROUTE = '/home'

export const ANIMAL_ROUTE = '/animals'
export const STAFF_ROUTE =  '/staffs' 
export const DOCUMENTATION_ROUTE = '/documents'
export const ITEM_ROUTE =  '/items'
export const ROOM_ROUTE = '/rooms'
export const ENCLOSURE_ROUTE = '/enclosures'

export const LOGIN_ROUTE = STAFF_ROUTE + '/login'
export const REGISTER_ROUTE = STAFF_ROUTE + '/register'
export const ADMIN_ROUTE = '/admins'
// export const CHECK_JWT_ROUTE = STAFF_ROUTE + '/check'

