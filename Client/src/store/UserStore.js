import { makeAutoObservable } from "mobx";

export default class UserStore {
    
    constructor() {
        this._isAuth = localStorage.getItem('isAuth') === 'true';
        this._isAdmin = localStorage.getItem('isAdmin') === 'true';
    
        this._user = {};

        makeAutoObservable(this);
        console.log('UserStore constructor');
    }

    setIsAuth(bool) {
        this._isAuth = bool;
    }

    setUser(user) {
        this._user = user;
    }

    setIsAdmin(bool) {
        this._isAdmin = bool;
    }


    get isAdmin() {
        return this._isAdmin;
    }

    get isAuth() {
        return this._isAuth;
    }

    get user() {
        return this._user;
    }
}
