import React, {useState} from "react";
import { Button, Container } from "react-bootstrap";
import Register from "../components/modals/Register";
import "../styles/default.css";
import { useNavigate } from "react-router-dom";
import { STAFF_ROUTE } from "../utils/constants";

    const Admin = () => {
    const [registerVisible, setRegisterVisible] = useState(false);
    const history = useNavigate()

    return(
        <Container className="d-flex flex-column">
            <Button className="mt-3" onClick={() => setRegisterVisible(true)}>Register new staff</Button>
            <Register show={registerVisible} onHide={() => setRegisterVisible(false)} />

            <Button className="mt-3" onClick={() => history(STAFF_ROUTE)}>Manage staffs</Button>
            
        </Container>
    );
    };

    export default Admin;