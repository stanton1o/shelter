import React, {  useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { readStaffById, deleteStaffById, updateStaffById } from "../http/staffAPI";
import { deleteFileById } from "../http/docAPI";
import {  STAFF_ROUTE } from "../utils/constants";
import { observer } from "mobx-react-lite";
import { Container, Spinner } from "react-bootstrap";
import { downloadClickHandler, replaceValues } from "../utils/functions";
import '../styles/staff.css';

const Staff = observer(() => {
    const [userData, setUserData] = useState({});
    const [dataLoaded, setDataLoaded] = useState(false);
    let [editedUserData, setEditedUserData] = useState({});
    const [selectedRoles, setSelectedRoles] = useState([]);
    const { id } = useParams();
    const history = useNavigate();
    
    useEffect(() => {
        readStaffById(id)
            .then(data => {
                setUserData(data);
            })
            .catch(error => {
                console.error("Error fetching user data:", error);
                history(STAFF_ROUTE);
            })
            .finally(() => {
                setDataLoaded(true);
            });
        }, [id, history]);
      

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setEditedUserData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleRoleChange = (event) => {
        const selectedRoles = Array.from(event.target.selectedOptions, option => option.value);
        setSelectedRoles(selectedRoles);
        editedUserData.roles = selectedRoles;
    };


    const handleDeleteFile = async (idDocument) => {
        try {
            await deleteFileById(idDocument);
            await readStaffById(id)
                .then(data => {
                    setUserData(data);
                })
                .catch(error => {
                    console.error("Error fetching user data after deleting file:", error);
                });
        } catch (error) {
            console.error("Error deleting file:", error);
        }
    };
    
    const handleDeleteStaff = async() => {
        try {
            await deleteStaffById(id);
            history(STAFF_ROUTE);
        }
        catch (error) {
            console.error("Error deleting staff:", error);
        }
    }

    return (
        <Container>
            {dataLoaded ? (
                <div className="staff-details-container" itemScope itemType="https://schema.org/Person">
                    <h2>Staff Details</h2>
                    <ul>
                        <li><strong>ID:</strong> {userData.idUser}</li>
                        <li>
                            <strong >Name:</strong>
                            <input
                                itemProp="givenName"
                                type="text"
                                name="name"
                                value={editedUserData.name || userData.name || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong >Surname:</strong>
                            <input
                                itemProp="familyName"
                                type="text"
                                name="surname"
                                value={editedUserData.surname || userData.surname || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong >Email:</strong>
                            <span itemProp="email">{userData.email}</span>
                        </li>
                        <li itemScope itemType="https://schema.org/DeliveryEvent">
                            <strong  itemProp="accessCode">Password:</strong>
                            <input
                                type="text"
                                name="password"
                                value={editedUserData.password || userData.password ||''}
                                onChange={handleInputChange}
                                required 
                            />
                        </li>
                        <li>
                            <strong >Phone:</strong>
                            <input
                                itemProp="telephone"
                                type="text"
                                name="phone"
                                value={editedUserData.phone || userData.phone || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li itemScope itemType="https://schema.org/MonetaryAmount" >
                            <strong >Salary:</strong>
                            <meta itemProp="currency" content="CZK" />
                            <input
                                itemProp="value"
                                type="number"
                                name="salary"
                                value={editedUserData.salary || userData.salary || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong >Documents:</strong>
                            <ul>
                                {userData.documents && userData.documents.map((document, index) => (
                                    <li key={index} itemScope itemType="http://schema.org/CreativeWork">
                                        <span itemProp="name">{document.name}</span> - <span itemProp="description">{document.description}</span>
                                        <button onClick={(e) => downloadClickHandler(e, document)}>Download</button>
                                        <button onClick={() => handleDeleteFile(document.idDocument)}>Delete</button>
                                    </li>
                                ))}
                            </ul>
                        </li>
                        <li >
                            <strong >Roles:</strong>
                            <select
                                name="roles"
                                multiple
                                value={selectedRoles}
                                onChange={handleRoleChange}
                            >
                                <option itemProp="jobTitle" value="USER" selected={selectedRoles.includes("USER")}>User</option>
                                <option itemProp="jobTitle" value="ADMIN" selected={selectedRoles.includes("ADMIN")}>Admin</option>
                            </select>
                        </li>

                        <li itemScope itemType='https://schema.org/DataFeedItem'>
                            <strong >Time of updated:</strong> 
                            <span itemProp="dateModified">{userData.timeOfUpdated}</span>
                        </li>
                    </ul>
                    <button onClick={() => {updateStaffById(id, replaceValues(userData, editedUserData))}}>Save Changes</button>
                    <button onClick={() => {handleDeleteStaff()}}>Delete</button>
                </div>
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
            )}
        </Container>
    );
    
});

export default Staff;