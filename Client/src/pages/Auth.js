import { observer } from "mobx-react-lite";
import React, {useContext, useState} from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import { Context } from '../index';
import { useNavigate } from "react-router-dom";
import { HOME_ROUTE } from "../utils/constants";
import {login, readMyself} from "../http/userAPI";

const Auth = observer(() => {
    const {user} = useContext(Context)
    const history = useNavigate()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const loginTo = async (e) => {
        e.preventDefault(); 
        try {
            const userData = await login(email, password);
            user.setUser(userData);
            user.setIsAuth(true);
            const staff = await readMyself();
            user.setIsAdmin(staff.roles.includes('ADMIN'));
            localStorage.setItem('isAuth', true);
            localStorage.setItem('isAdmin', user.isAdmin);
            history(HOME_ROUTE);
        
        } catch (error) {
            if(error.code === 'ERR_NETWORK')
                alert('Network error, please try again later');
            else
                alert('Login or password is invalid'); 
        }
    
    };

    return (
        <Container>
            <Card className="mt-4" style={{ width: "50%", margin: "0 auto" }}>
                <Card.Body>
                    <Card.Title>Authorization</Card.Title>
                     
                    <Form > 
                        <Form.Group controlId="formEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" 
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                autoComplete="username"
                            />
                        </Form.Group>

                        <Form.Group controlId="formPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Enter password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                autoComplete="current-password"
                            />
                        </Form.Group>

                        <Button 
                        type="submit" 
                        style={{marginTop: '10px'}}
                        onClick={loginTo}>
                            Login
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        </Container>
    );
});

export default Auth;
