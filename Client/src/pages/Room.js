import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Container, Spinner } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import { deleteRoomById, readEnclosureById, readRoomById, updateEnclosure, updateRoom } from "../http/roomsAPI";
import { ANIMAL_ROUTE, ITEM_ROUTE, ROOM_ROUTE } from "../utils/constants";
import { deleteItemById } from "../http/itemAPI";
import { replaceValues } from "../utils/functions";
import '../styles/room.css';
const Room = observer(() => {
    const [dataLoaded, setDataLoaded] = useState(false);
    const [dataRoom, setDataRoom] = useState({});
    let [editedDataRoom, setEditedDataRoom] = useState({});
    const [isEnclosure, setIsEnclosure] = useState(false); // Добавленное состояние isEnclosure
    const history = useNavigate();
    const {id} = useParams();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const room = await readEnclosureById(id);
                setDataRoom(room);
                setIsEnclosure(true);          
            }catch (error) {
                try {
                    const room = await readRoomById(id);
                    setDataRoom(room);
                } catch (error) {
                    console.error("Error fetching room data:", error);
                    history(ROOM_ROUTE);
                }
            } finally {
                setDataLoaded(true);
            }
        };
        fetchData();
    }, [id, history]);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setEditedDataRoom(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const deleteItemHandler = async (idItem) => {
        try {
            await deleteItemById(idItem);
            await readRoomById(id)
                .then(data => {
                    setDataRoom(data);
                })
                .catch(error => {
                    console.error("Error fetching room data after deleting item:", error);
                });
        } catch (error) {
            console.error("Error deleting item:", error);
        }
    }

    const deleteRoomHandler = async () => {
        try {
            if(isEnclosure) 
                if(dataRoom.animals.length > 0)
                    return alert("You can't delete room with animals");
            if(dataRoom.items.length > 0)
                return alert("You can't delete room with items");
            await deleteRoomById(id);
            history(ROOM_ROUTE);
        } catch (error) {
            console.error("Error deleting room:", error);
        }
    }

    const updateRoomHandler = async () => {
        try {
            editedDataRoom = replaceValues(dataRoom, editedDataRoom);
           if(isEnclosure) 
                await updateEnclosure(id, editedDataRoom);
            else
                await updateRoom(id, editedDataRoom);
           
        } catch (error) {
            console.error("Error updating room:", error);
        }
    }
    return (
        <Container>
            {dataLoaded ? (
            <div itemScope itemType="https://schema.org/Room" className="room-details-container">
                <h2>{isEnclosure ? 'Enclosure details' : 'Room details'}</h2>
                <ul>
                    <li><strong>Id:</strong>{dataRoom.idRoom}</li>
                    <li>
                        <strong>Name:</strong>
                        <input
                            itemProp="name"
                            type="text"
                            name="name"
                            value={editedDataRoom.name || dataRoom.name || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li>
                        <strong>Description</strong>:
                        <input
                            itemProp="description"
                            type="text"
                            name="description"
                            value={editedDataRoom.description || dataRoom.description || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li>
                        <strong>Area:</strong>
                        <input
                            itemProp="floorSize"
                            type="number"
                            name="area"
                            value={editedDataRoom.area || dataRoom.area || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li>
                        <strong>High:</strong>
                        <input
                            type="number"
                            name="high"
                            value={editedDataRoom.high || dataRoom.high || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li className="room-details-container-nested" itemScope itemType="https://schema.org/ListItem">
                        <strong>Items:</strong>
                        <ul>
                                {dataRoom.items && dataRoom.items.map((i, index) => (
                                    <li key={index}>
                                        <span itemProp="item">{i.name} - {i.description}</span>
                                        <button onClick={() => history(ITEM_ROUTE + `/${i.id}`)}>Show</button>
                                        <button onClick={() => deleteItemHandler(i.id) }>Delete</button>
                                    </li>
                                ))}
                            </ul>
                    </li>
                    <li itemScope itemType='https://schema.org/DataFeedItem'>
                            <strong >Time of updated:</strong> 
                            <span itemProp="dateModified">{dataRoom.timeOfUpdated}</span>
                        </li>
                    {isEnclosure && 
                    <li className="room-details-container-nested" itemProp="petsAllowed">
                        <strong>Animals:</strong>
                        <ul>
                            {dataRoom.animals && dataRoom.animals.map((a, index) => (
                                <li key={index}>
                                    {a.name} - {a.species}
                                    <button onClick={() => history(ANIMAL_ROUTE + `/${a.id}`)}>Show</button>
                                </li>
                            ))}
                        </ul>
                    </li>}
                </ul>
                <button onClick={() => {deleteRoomHandler()}}>Delete</button>
                <button onClick={() => updateRoomHandler()}>Save changes</button>
            </div>
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
            )}
        </Container>
    );
});

export default Room;
