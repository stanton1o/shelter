import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { Container, Spinner } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import {  ITEM_ROUTE } from "../utils/constants";
import { deleteItemById, readItemById, updateItem } from "../http/itemAPI";
import { replaceValues } from "../utils/functions";
import { readAllRooms } from "../http/roomsAPI";
import '../styles/item.css';

const Item = observer(() => {
    const [dataLoaded, setDataLoaded] = useState(false);
    const [dataItem, setDataItem] = useState({});
    const [rooms, setRooms] = useState([]);
    let [editedDataItem, setEditedDataItem] = useState({});
    const history = useNavigate();
    const {id} = useParams();

    useEffect(() => {
        const fetchData = async () => {
            try {
                const item = await readItemById(id);
                setDataItem(item);
                setRooms(await readAllRooms());
            }catch (error) {
                console.error("Error fetching item data:", error);
                history(ITEM_ROUTE);
            } finally {
                setDataLoaded(true);
            }
        };
        fetchData();
    }, [id, history]);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setEditedDataItem(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const updateItemHandler = async () => {
        try {
            await updateItem(id, replaceValues(dataItem, editedDataItem));
            const item = await readItemById(id);
            setDataItem(item);
        } catch (error) {
            console.error("Error updating item:", error);
        }
    }
    const deleteItemHandler = async () => {
        try {
            await deleteItemById(id);
            history(ITEM_ROUTE);
        } catch (error) {
            console.error("Error deleting item:", error);
        }
    }
  
    return (
        <Container>
            {dataLoaded ? (
            <div className="item-details-container" itemScope itemType="https://schema.org/Thing">
                <h2>Item details</h2>
                <ul>
                    <li><strong>Id: </strong>{dataItem.idItem}</li>
                    <li>
                        <strong>Name </strong>
                        <input
                            itemProp="name"
                            type="text"
                            name="name"
                            value={editedDataItem.name || dataItem.name || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li>
                        <strong>Description </strong>
                        <input
                            itemProp="description"
                            type="text"
                            name="description"
                            value={editedDataItem.description || dataItem.description || ''}
                            onChange={handleInputChange}
                            required
                        />
                    </li>
                    <li><strong>Id room </strong>
                            <select
                                name="idRoom"
                                value={editedDataItem.idRoom || dataItem.idRoom || ''}
                                onChange={handleInputChange}
                                required
                            >
                                <option value={0}>Select a room</option>
                                {rooms.map(room => (
                                    <option key={room.id} value={room.id}>{room.name}</option>
                                ))}
                            </select>
                    </li>
                    <li itemScope itemType='https://schema.org/DataFeedItem'>
                            <strong >Time of updated: </strong> 
                            <span itemProp="dateModified">{dataItem.timeOfUpdated}</span>
                        </li>
                </ul>
                <button onClick={() => {deleteItemHandler()}}>Delete</button>
                <button onClick={() => updateItemHandler()}>Save changes</button>
            </div>
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
            )}
        </Container>
    );
});

export default Item;
