import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { deleteAnimalById, readAnimalById, readStatuses, updateAnimalById } from "../http/animalAPI";
import { ANIMAL_ROUTE } from "../utils/constants";
import { Container, Spinner } from "react-bootstrap";
import { readAllEnclosures } from "../http/roomsAPI";
import { downloadClickHandler, replaceValues } from "../utils/functions";
import { deleteFileById } from "../http/docAPI";
import { uploadFile } from "../http/staffAPI";
import { readMyself } from "../http/userAPI";
import "../styles/animal.css";


const Animal = observer(() => {
    const [animalData, setAnimalData] = useState({});
    const [dataLoaded, setDataLoaded] = useState(false);
    let [editedAnimalData, setEditedAnimalData] = useState({});
    const [statuses, setStatuses] = useState([])
    const [rooms, setRooms] = useState([])
    
    const { id } = useParams();
    const history = useNavigate();
   
    useEffect(() => {
        const fetchData = async () => {
            try {
                setAnimalData(await readAnimalById(id));
                const roomsData = await readAllEnclosures();
                if (roomsData.length === 0) {
                    alert("No rooms available. Please create a room first.");
                }
                setRooms(roomsData);
                setStatuses(await readStatuses())
            } catch (error) {
                console.error("Error fetching data:", error);
                history(ANIMAL_ROUTE);
            } finally {
                setDataLoaded(true);
            }
        };

        fetchData();
        }, [id, history]);

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setEditedAnimalData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleDeleteAnimal = async() => {
        try {
            await deleteAnimalById(id);
            history(ANIMAL_ROUTE);
        } catch (error) {
            console.error("Error deleting animal:", error);
        }
        
    }

    const handleDeleteFile = async (idDocument) => {
        try {
            await deleteFileById(idDocument);
            await readAnimalById(id)
                .then(data => {
                    setAnimalData(data);
                })
                .catch(error => {
                    console.error("Error fetching animal data after deleting file:", error);
                });
        } catch (error) {
            console.error("Error deleting file:", error);
        }
    };

    const fileUpload = async (file) => {
        try {
            const formData = new FormData(); 
            formData.append('file', file); 
            const me = await readMyself();
            console.log(me);
            console.log(id);
            await uploadFile(formData,me.idUser, id); 
            await readAnimalById(id) 
                .then(data => {
                    setAnimalData(data);
                })
                .catch(error => {
                    console.error("Error fetching animal data after uploading file:", error);
                });
        } catch (error) {
            console.error("Error uploading file:", error);
        }
    };

    const handleFileUpload = (e) => {
        const files = e.target.files;
        for (let i = 0; i < files.length; i++) {
            fileUpload(files[i]);
        }
        e.target.value = '';
    }

    return (
        <Container>
            {dataLoaded ? (
                <div className="animal-details-container" itemScope itemType="https://schema.org/Person">
                    <h2>Animal Details</h2>
                    <ul>
                        <li><strong>ID:</strong> {animalData.idAnimal}</li>
                        <li><strong>Room ID:</strong>
                            <select
                                name="idRoom"
                                value={editedAnimalData.idRoom || animalData.idRoom || ''}
                                onChange={handleInputChange}
                                required
                            >
                                <option value={0}>Select a room</option>
                                {rooms.map(room => (
                                    <option key={room.id} value={room.id}>{room.name}</option>
                                ))}
                            </select>
                        </li>
                        <li>
                            <strong>Name:</strong> 
                            <input
                                itemProp="name"
                                type="text"
                                name="name"
                                value={editedAnimalData.name || animalData.name || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Species:</strong>
                            <input
                                itemProp="additionalType"
                                type="text"
                                name="species"
                                value={editedAnimalData.species || animalData.species || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Breed:</strong> 
                            <input
                                itemProp="additionalType"
                                type="text"
                                name="breed"
                                value={editedAnimalData.breed || animalData.breed || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Birth Date:</strong>
                            <input
                                itemProp="birthDate"
                                type="datetime-local"
                                name="birthDate"
                                value={editedAnimalData.birthDate || animalData.birthDate || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Arrival Date:</strong>
                            <input
                                type="datetime-local"
                                name="arrivalDate"
                                value={editedAnimalData.arrivalDate || animalData.arrivalDate || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Sex:</strong>
                            <select
                                itemProp="gender"
                                name="sex"
                                value={editedAnimalData.sex || animalData.sex || ''}
                                onChange={handleInputChange}
                                required
                            >
                                <option value="MALE" selected={animalData.sex === 'MALE'}>Male</option>
                                <option value="FEMALE" selected={animalData.sex === 'FEMALE'}>Female</option>
                            </select>
                        </li>
                        <li>
                            <strong>Description:</strong> 
                            <input
                                itemProp="description"
                                type="text"
                                name="description"
                                value={editedAnimalData.description || animalData.description || ''}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li>
                            <strong>Weight:</strong> 
                            <input
                                itemProp="weight"
                                type="number"
                                name="weight"
                                value={editedAnimalData.weight || animalData.weight || 0}
                                onChange={handleInputChange}
                                required
                            />
                        </li>
                        <li itemScope itemType="https://schema.org/Action">
                            <strong>Status:</strong> 
                            <select
                                itemProp="actionStatus"
                                name="status"
                                value={editedAnimalData.status || animalData.status || ''}
                                onChange={handleInputChange}
                                required
                            >
                                <option value={0}>Select a status</option>
                                {statuses.map(s => (
                                    <option key={s} value={s}>{s}</option>
                                ))}
                            </select>
                        </li>
                        <li className="animal-details-container-nested">
                            <strong>Documents:</strong>
                            <ul>
                                {animalData.documents && animalData.documents.map((document, index) => (
                                    <li key={index} temScope itemType="http://schema.org/CreativeWork">
                                         <span itemProp="name">{document.name}</span> - <span itemProp="description">{document.description}</span>
                                        <button onClick={(e) => downloadClickHandler(e, document)}>Download</button>
                                        <button onClick={() => handleDeleteFile(document.idDocument)}>Delete</button>
                                    </li>
                                ))}
                                {/* <label htmlFor="fileUp">Choose a file to upload:</label>
                                <input id="fileUp" className="file-upload-btn" multiple={true} type="file" onChange={(e) => handleFileUpload(e)}/> */}
                                <div className="file-input-container">
                                    <input id="fileUp" className="file-input" multiple={true} type="file" onChange={(e) => handleFileUpload(e)} />
                                    <label htmlFor="fileUp">Choose a file to upload</label>
                                </div>

                            </ul>
                        </li>
                        <li itemScope itemType='https://schema.org/DataFeedItem'>
                            <strong >Time of updated: </strong> 
                            <span itemProp="dateModified">{animalData.timeOfUpdated}</span>
                        </li>
                    </ul>
                    <button onClick={() => {updateAnimalById(id, replaceValues(animalData, editedAnimalData))}}>Save Changes</button>
                    <button onClick={() => {handleDeleteAnimal()}}>Delete</button>
                </div>
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
            )}
        </Container>
    );
    
});

export default Animal;