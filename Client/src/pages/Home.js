import React, { useState } from "react";
import { Container, ListGroup, Row } from 'react-bootstrap';
import "../styles/home.css";
import { useNavigate } from 'react-router-dom'; 
import { ANIMAL_ROUTE, ITEM_ROUTE, ROOM_ROUTE } from "../utils/constants";
import AddAnimal from "../components/modals/AddAnimal";
import AddRoom from "../components/modals/AddRoom";
import AddItem from "../components/modals/AddItem";

const Home = () => {
    const history = useNavigate()
    const [animalVisible, setAnimalVisible] = useState(false);
    const [roomVisible, setRoomVisible] = useState(false);
    const [itemVisible, setItemVisible] = useState(false);

    return (
        <Container className="d-flex flex-column">
            <ListGroup className="home-list">
                <Row md={3}>
                    
                    <ListGroup.Item className="home-list-item" onClick={() => history(ANIMAL_ROUTE)}>Show animals</ListGroup.Item>
                    <ListGroup.Item className="home-list-item" onClick={() => setAnimalVisible(true)}>Add animal</ListGroup.Item>
                    {animalVisible && <AddAnimal show={animalVisible} onHide={() => setAnimalVisible(false)} />}

                    <ListGroup.Item className="home-list-item" onClick={() => history(ROOM_ROUTE)}>Show rooms</ListGroup.Item>
                    <ListGroup.Item className="home-list-item" onClick={() => setRoomVisible(true)}>Add room</ListGroup.Item>
                    <AddRoom show={roomVisible} onHide={() => setRoomVisible(false)} />

                    <ListGroup.Item className="home-list-item" onClick={() => history(ITEM_ROUTE)}>Show items</ListGroup.Item>
                    <ListGroup.Item className="home-list-item" onClick={() => setItemVisible(true)}>Add item</ListGroup.Item>
                    {itemVisible && <AddItem show={itemVisible} onHide={() => setItemVisible(false)} />}
                </Row>
            </ListGroup>     
        </Container>
    );
};

export default Home;
