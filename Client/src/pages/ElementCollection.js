import { Container, FormLabel, Row, Spinner } from "react-bootstrap";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import {  useLocation } from "react-router-dom";
import { showAll } from "../http/userAPI";
import Element from "../components/Element";

const ElementCollection = observer(() => {
    const [elements, setElements] = useState([]);
    const [dataLoaded, setDataLoaded] = useState(false);
    const location = useLocation();

    useEffect(() => {
        const fetchElements = async () => {
            try {
                const elementsData = await showAll(location.pathname);
                setElements(elementsData);
                setDataLoaded(true);
            } catch (error) {
                console.error("Error fetching elements:", error);
            }
        };

        fetchElements();
    }, [location]);

    return (
        <Container>
            {dataLoaded ? (
                
                elements.length === 0 ? (
                    <FormLabel 
                        className="d-flex justify-content-center align-items-center" 
                        style={{ marginTop: '30%', fontSize: '2.5rem' }}>
                        No elements found :(
                    </FormLabel>
                ) : (
                    <Row className="d-flex">
                        {elements.map(element =>
                            <Element key={element.id} element={element} />
                        )}
                    </Row>
                )
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
            )}
        </Container>
    );
    
});

export default ElementCollection;
