import React from "react";
import { Card, Col } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

const Element = ({element}) => {
    const history = useNavigate()
    const location = useLocation()
    return (
        <Col md={4} className="mt-3" onClick={() => history(location.pathname + '/' + element.id)}>
            <Card className="mt-3">
                <Card.Body>
                {Object.entries(element).map(([key, value]) => (
                    <div key={key}>
                        <strong>{key}: </strong>
                        {typeof value === 'object' ? JSON.stringify(value) : String(value)}
                    </div>
                ))}     
                </Card.Body>
            </Card>
        </Col>
    );
};

export default Element;

// {Object.entries(element).map(([key, value]) => (
//     <div key={key}>
//         <strong>{key}: </strong>
//         {value}
//     </div>
// ))}