import React, {useCallback, useContext, useEffect} from 'react';
import {Context} from '../index';
import { Navbar, Container, Nav, Button } from 'react-bootstrap';
import "../styles/default.css"
import { observer } from 'mobx-react-lite';
import { useLocation, useNavigate  } from 'react-router-dom';
import { ADMIN_ROUTE, HOME_ROUTE, LOGIN_ROUTE } from '../utils/constants';
import { validate } from '../http/userAPI';
export const NavBar = observer(() => {
    const {user} = useContext(Context)
    const history = useNavigate()
    const location = useLocation()

    const handleLogout = useCallback(() => {
        user.setUser({});
        user.setIsAuth(false);
        user.setIsAdmin(false);
        localStorage.clear();
        alert('You have been logged out. Please log in again.');
    }, [user]);

    useEffect(() => {
        const fetchValidation = async () => {
            try {
                await validate();
            } catch (error) {
                if(error.code === 'ERR_NETWORK')
                    alert('Network error, please try again later');
                else if(location.pathname !== LOGIN_ROUTE) {
                    handleLogout();
                }
            }
        };
        fetchValidation();
    }, [location, handleLogout]);
    return (
        <Navbar bg="dark" data-bs-theme="dark">
            <Container>
                {<Navbar.Brand style={{color: 'whitesmoke', scale:'1.7'}}>Shelter</Navbar.Brand>}
                {
                    user.isAuth 
                    && <Nav className="ml-auto">
                        <Button className="mt-3" onClick={() => history(HOME_ROUTE)}>Home page</Button>
                        {user.isAdmin && <Button className="mt-3" onClick={() => history(ADMIN_ROUTE)}>Admin page</Button>}
                        <Button className="mt-3" onClick={handleLogout}>Logout</Button>
                    </Nav>
                    
                }
            </Container>
        </Navbar>
    );
});