import React, {  useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import "../../styles/default.css";
import { createEnclosure, createRoom } from "../../http/roomsAPI";

const AddRoom = ({ show, onHide }) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [area, setArea] = useState(1);
    const [high, setHigh] = useState(1);
    const [isEnclosure, setIsEnclosure] = useState(false);

    const create = async (e) => {
        e.preventDefault();
        try {
            const room = {name, description, area, high}
            console.log(room);
            if(isEnclosure)
                await createEnclosure(room);
            else
                await createRoom(room);
            onHide();
        } catch (error) {
            alert('You must to fill all fields');
        }
    };

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add new room
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form className="d-flex flex-column">
                    <Form.Control className="mt-3" placeholder="Enter room's name:"
                        value={name}
                        type="text"
                        onChange={(e) => setName(e.target.value)}
                        required />
                    
                    <Form.Control className="mt-3" placeholder="Enter room's description:"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required />
                    <Form.Label className="mt-3">Enter room's area:
                        <Form.Control className="mt-3" placeholder="Enter room's area:"
                            value={area}
                            type="number"
                            onChange={(e) => setArea(e.target.value)}
                            required />
                    </Form.Label>
                    <Form.Label className="mt-3">Enter room's high: 
                        <Form.Control className="mt-3" placeholder="Enter room's high:"
                            value={high}
                            type="number"
                            onChange={(e) => setHigh(e.target.value)}
                            required />
                    </Form.Label>
                    <Form.Check className="mt-3" type="checkbox" label="Is enclosure?"
                        value={isEnclosure}
                        onChange={(e) => setIsEnclosure(e.target.checked)}
                        required />
                </Form>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="outline-danger" style={{ backgroundColor: "darkred" }} onClick={onHide}>Close</Button>
                <Button variant="outline-success" style={{ backgroundColor: "darkgreen" }} onClick={create}>Add room</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default AddRoom;
