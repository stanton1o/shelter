import React, { useEffect, useState } from "react";
import { Button, Modal, Form, Spinner } from "react-bootstrap";
import "../../styles/default.css";
import { createAnimal, readStatuses } from "../../http/animalAPI";
import { readAllEnclosures } from "../../http/roomsAPI";

const AddAnimal = ({ show, onHide }) => {
    const [name, setName] = useState("");
    const [idRoom, setIdRoom] = useState(0);
    const [species, setSpecies] = useState("");
    const [breed, setBreed] = useState("");
    const [birthDate, setBirthDate] = useState("");
    const [arrivalDate, setArrivalDate] = useState("");
    const [sex, setSex] = useState("MALE");
    const [description, setDescription] = useState("");
    const [weight, setWeight] = useState(0);
    const [status, setStatus] = useState("");
    const [rooms, setRooms] = useState([]);
    const [statuses, setStatuses] = useState([]);
    const [dataLoaded, setDataLoaded] = useState(false);

    useEffect(() => {
        const fetchArrays = async () => {
            try {
                const roomsData = await readAllEnclosures();
                if (roomsData.length === 0) {
                    alert("No rooms available. Please create a room firs and add a new animal after.");
                    onHide();
                }
                setRooms(roomsData);
                setStatuses(await readStatuses())
            } catch (error) {
                console.error("Error fetching rooms:", error);
            }
        };

        fetchArrays();
        setDataLoaded(true);
    }, [onHide]);

    const create = async (e) => {
        e.preventDefault();
        try {
            const animal = {name, idRoom, species, breed, birthDate, arrivalDate, sex, description, weight, status}
            console.log(animal);
            await createAnimal(animal);
            onHide();
        } catch (error) {
            alert('You must to fill all fields');
        }
    };

    return (
        
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add new animal
                </Modal.Title>
            </Modal.Header>
            {dataLoaded ? (
            <Modal.Body>
                <Form className="d-flex flex-column">
                    <Form.Control className="mt-3" placeholder="Enter animal's name:"
                        value={name}
                        type="text"
                        onChange={(e) => setName(e.target.value)}
                        required />
                    <Form.Control className="mt-3" as="select" value={idRoom} onChange={(e) => setIdRoom(e.target.value)}
                        required>
                        <option value={0}>Select a room</option>
                        {rooms.map(room => (
                            <option key={room.id} value={room.id}>{room.name}</option>
                        ))}
                    </Form.Control>
                    <Form.Control className="mt-3" placeholder="Enter animal's species:"
                        value={species}
                        type="text"
                        onChange={(e) => setSpecies(e.target.value)}
                        required />
                    <Form.Control className="mt-3" placeholder="Enter animal's breed:"
                        value={breed}
                        type="text"
                        onChange={(e) => setBreed(e.target.value)}
                        required />
                    <Form.Label className="mt-3">Birth date:
                        <Form.Control className="mt-3" 
                            value={birthDate}
                            type="datetime-local"
                            onChange={(e) => setBirthDate(e.target.value)}
                            required />
                    </Form.Label>
                    <Form.Label className="mt-3">Arrival date:
                        <Form.Control className="mt-3"
                            value={arrivalDate}
                            type="datetime-local"
                            onChange={(e) => setArrivalDate(e.target.value)}
                            required />
                    </Form.Label>
                    <Form.Select className="mt-3" aria-label="Select roles"
                    value={sex} required onChange={(e) => setSex(e.target.value)}>
                        <option value="MALE">MALE</option>
                        <option value="FEMALE">FEMALE</option>
                    </Form.Select>
                    <Form.Control className="mt-3" placeholder="Enter animal's description:"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required />
                    <Form.Label className="mt-3">Weight:
                        <Form.Control className="mt-3" 
                            value={weight}
                            type="number"
                            onChange={(e) => setWeight(e.target.value)}
                            required />
                    </Form.Label>
                     <Form.Control className="mt-3" as="select" value={status} onChange={(e) => setStatus(e.target.value)}
                        required>
                        <option value={0}>Select a status</option>
                        {statuses.map(s => (
                            <option key={s} value={s}>{s}</option>
                        ))}
                    </Form.Control>
                </Form>
            </Modal.Body>
            ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
                )}
            <Modal.Footer>
                <Button variant="outline-danger" style={{ backgroundColor: "darkred" }} onClick={onHide}>Close</Button>
                <Button variant="outline-success" style={{ backgroundColor: "darkgreen" }} onClick={create}>Add animal</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default AddAnimal;
