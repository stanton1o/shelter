import React, {  useEffect, useState } from "react";
import { Button, Modal, Form, Spinner } from "react-bootstrap";
import "../../styles/default.css";
import {  readAllRooms } from "../../http/roomsAPI";
import { createItem } from "../../http/itemAPI";

const AddItem = ({ show, onHide }) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [idRoom, setIdRoom] = useState(0);
    const [dataLoaded, setDataLoaded] = useState(false);
    const [rooms, setRooms] = useState([]);

    useEffect(() => {
        const fetchArrays = async () => {
            try {
                const dataRooms = await readAllRooms();
                if(dataRooms.length === 0) {
                    alert("No rooms available. Please create a room first and add a new item after.");
                    onHide();
                }
                setRooms(dataRooms);
            } catch (error) {
                console.error("Error fetching rooms:", error);
            }
            
        }
        fetchArrays();
        setDataLoaded(true);
    }, [onHide,rooms]);

    const create = async (e) => {
        e.preventDefault();
        try {
            const item = {name, description,idRoom}
            console.log(item);
            await createItem(item);
            onHide();
        } catch (error) {
            alert('You must to fill all fields');
        }
    };

    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Add new item
                </Modal.Title>
            </Modal.Header>
            {dataLoaded ? (
            <Modal.Body>
                <Form className="d-flex flex-column">
                    <Form.Control className="mt-3" placeholder="Enter item's name:"
                        value={name}
                        type="text"
                        onChange={(e) => setName(e.target.value)}
                        required />
                    
                    <Form.Control className="mt-3" placeholder="Enter item's description:"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        required />
                    <Form.Control className="mt-3" as="select" value={idRoom} onChange={(e) => setIdRoom(e.target.value)}
                        required>
                        <option value={0}>Select a room</option>
                        {rooms.map(room => (
                            <option key={room.id} value={room.id}>{room.name}</option>
                        ))}
                    </Form.Control>
                </Form>
            </Modal.Body>
             ) : (
                <Spinner animation="border" style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                }} />
                )}
            <Modal.Footer>
                <Button variant="outline-danger" style={{ backgroundColor: "darkred" }} onClick={onHide}>Close</Button>
                <Button variant="outline-success" style={{ backgroundColor: "darkgreen" }} onClick={create}>Add item</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default AddItem;
