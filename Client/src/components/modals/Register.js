import React, {useState} from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { registration } from "../../http/userAPI";
import "../../styles/default.css";
const Register = ({show, onHide}) => {
 
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [roles, setRoles] = useState([])


    const registrate = async (e) => {
        e.preventDefault(); 
        try {
            await registration(name, surname, email, password, roles);
            onHide();
        } catch (error) {
             if (error.response.status === 403 || error.response.status === 409) {
                alert('This email is reserved or already exists');
            } else{
                alert(error.response.data.message);
            } 
        }
    };
    return (
        <Modal
            show={show}
            onHide={onHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Register new staff
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form className="d-flex flex-column">
                    <Form.Control className="mt-3" placeholder="Enter staff's name:" 
                    value={name}
                    onChange={e => setName(e.target.value)}/>
                    <Form.Control className="mt-3" placeholder="Enter staff's surname:" 
                    value={surname}
                    onChange={e => setSurname(e.target.value)}/>
                    <Form.Control className="mt-3" placeholder="Enter staff's email:" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}/>
                    <Form.Control className="mt-3" placeholder="Enter staff's password:" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}/>
                    <Form.Select className="mt-3" aria-label="Select roles" multiple
                    value={roles} onChange={(e) => setRoles(Array.from(e.target.selectedOptions, (option) => option.value))}>
                        <option value="USER">User</option>
                        <option value="ADMIN">Admin</option>
                    </Form.Select>

                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-danger" style={{backgroundColor: "darkred"}} onClick={onHide}>Close</Button>
                <Button variant="outline-success" style={{backgroundColor: "darkgreen"}} onClick={registrate}>Registrate</Button>
            </Modal.Footer>
        </Modal>
    );
};

export default Register;
