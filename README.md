Shelter tutorial
===========================

DataBase connect
-----------------------
1. `url=jdbc:postgresql://localhost:5432/shelter`
2. `username=ovchiant`
3. `password=1243`

OR

1. `Docker run: 'docker run --name shelter_16.2 -p 5432:5432 -e POSTGRES_USER=ovchiant -e POSTGRES_PASSWORD=1243 -e POSTGRES_DB=shelter -d postgres:alpine3.19'`

How to run this application
---------------------------
1. `Enable Database`
2. `Run ShelterAplication to start the server`
3. `Run client to start the web client ('npm start' in folder client)`
4. `Open` http://localhost:3000/staffs/login `if it didn't happen automatically`
5. `'ADMIN@ADMIN' is a login or your own`
6. `'ADMIN' is a password or your own`


API documentation
-----------------

* `Open` http://localhost:8080/info `(When aplication is running)`

OR

* `Open ` https://editor.swagger.io/
* `File -> Import file: 'API Documentation.json'`


Server
------
* `Server is running on port 8080`
* `Java: 17`
* `Spring Boot: 3.2.3`
* `Dependencies (Gradle):`
    + `implementation 'org.springframework.boot:spring-boot-starter-data-jpa'`
	+ `implementation 'org.springframework.boot:spring-boot-starter-web'`
	+ `runtimeOnly 'org.postgresql:postgresql'`
	+ `testImplementation 'org.springframework.boot:spring-boot-starter-test'`
	+ `implementation 'org.springframework.boot:spring-boot-starter-validation'`
	+ `implementation "org.springframework.boot:spring-boot-starter-security"`
	+ `compileOnly "org.projectlombok:lombok:${lombokVersion}"`
	+ `annotationProcessor "org.projectlombok:lombok:${lombokVersion}"`
	+ `implementation "org.mapstruct:mapstruct:${mapstructVersion}"`
	+ `annotationProcessor "org.mapstruct:mapstruct-processor:${mapstructVersion}"`
	+ `testAnnotationProcessor "org.mapstruct:mapstruct-processor:${mapstructVersion}"`
	+ `annotationProcessor "org.projectlombok:lombok-mapstruct-binding:${lombokMapstructBindingVersion}"`
	+ `implementation 'io.jsonwebtoken:jjwt-api:0.11.5'`
	+ `implementation 'io.jsonwebtoken:jjwt-impl:0.11.5'`
	+ `implementation 'io.jsonwebtoken:jjwt-jackson:0.11.5'`
	+ `implementation 'org.springdoc:springdoc-openapi-starter-webmvc-ui:2.5.0'`
	+ `testImplementation 'com.h2database:h2'`


Client
------
* `Client is running on port 3000`
* `NodeJS version: v20.12.2`
* `Npm version: 10.5.0`
* `Dependencies:`
    + `"axios": "^1.6.8",`
    + `"express-fileupload": "^1.5.0",`
    + `"jwt-decode": "^4.0.0",`
    + `"mobx": "^6.12.3",`
    + `"mobx-react-lite": "^4.0.7",`
    + `"react": "^18.2.0",`
    + `"react-bootstrap": "^2.10.2",`
    + `"react-dom": "^18.2.0",`
    + `"react-router-dom": "^6.22.3",`
    + `"react-scripts": "5.0.1",`
    + `"web-vitals": "^2.1.4"`
* `devDependencies:`
    + `"@babel/plugin-proposal-private-property-in-object": "^7.21.11"`
    

        
